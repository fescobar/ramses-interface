"""
This file implements and extends the control algorithm from

    G. Pierrou, H. Lai, G. Hug, and X. Wang, ‘A decentralized wide-area voltage
    control scheme for coordinated secondary voltage regulation using PMUs’,
    IEEE Trans. Power Syst., pp. 1–13, 2024, DOI: 10.1109/TPWRS.2024.3381553.

This program relies on functions and classes located in the directory
../../src. These functions and classes handle all aspects of the simulations,
from initializing the power flow cases to processing the results.

Francisco Escobar (fescobar@ethz.ch)
October 2024, Zurich

TO DO:
    - plot the instantaneous operating points in the PQ plane to check that
      they're inside the FOR.
"""

# Modules from the standard library
from typing import Type
import os
import sys
from itertools import zip_longest
from bisect import insort

# Modules from this repository
sys.path.append(os.path.join("..", "..", "src"))
from control import Controller
from experiment import Experiment, write_log
from metrics import Metric
from pf_dynamic import System
from records import Bus, Injector, DERA
from sim_interaction import Disturbance, Observable
from stochastic import (
    get_IEEE_39,
    get_base_experiment,
    RestorativeLoad,
    Voltages,
    LoadPowers,
)
from utils import Timer
from visual import Visualization

# Third-party modules
from scipy.optimize import Bounds, minimize, NonlinearConstraint
import numpy as np
import pyramses
import matplotlib.pyplot as plt

# A central concept of this program is that of the capability curves, known
# elsewhere in the literature as feasibility operating regions. These are given
# a class of their own. We assume that the regions are computed offline
# (meaning, in our simulation world, that they are computed by another program)
# and hence this class only needs to load existing data, probably from
# plain-text files.


class CapabilityCurve:
    """
    A data structure for capability curves of distribution networks.

    Other classes rely on a method called get_Q_inj_Mvar_limits().

    This class works with Mvar, and not with pu, because these units make the
    most sense for interfacing quantities from different networks, which may
    have different base powers.
    """

    pass


class ExampleCurve(CapabilityCurve):
    """A capability curve for testing purposes."""

    def get_Q_inj_Mvar_limits(
        self,
        P_inj_MW: float,
        V_pu: float,
    ) -> tuple[float]:

        Q_Mvar_min = -1000
        Q_Mvar_max = 1000

        return Q_Mvar_min, Q_Mvar_max


# Another central concept is that of an area. This is a set of buses in a power
# system that are close to each other according to a metric of electrical
# distance.


class Area:
    """A data structure for the areas of a power system."""

    def __init__(
        self,
        system: System,
        uncontrolled_load_bus_names: list[str],
        pilot_names: list[str],
        controlled_classes: list[Type[Injector]],
        pilot_V_pu_min: dict[str, float],
        pilot_V_pu_max: dict[str, float],
        capability_curves: dict[str, CapabilityCurve],
        name: str,
    ) -> None:
        """
        Create an area.

        Arguments
        ----------
        uncontrolled_load_bus_names: list[str]
            Names of buses in this area that feed a dynamic load but are not
            controlled.
        pilot_names: list[str]
            Name of buses in this area that feed a dynamic load and are
            controlled.
        controlled_classes: list[Type[Injector]]
            A list of classes, the instances of which will be controlled if
            they are connected to the (i.e. downstream of) pilot bus.
            Importantly, this is not a list of instances, but of classes
            themselves.
        pilot_V_pu_min: dict[str, float]
            Minimum voltages of the pilot buses (pu).
        pilot_V_pu_max: dict[str, float]
            Maximum voltages of the pilot buses (pu).
        capability_curves: dict[str, CapabilityCurve]
            Capability curves of the distribution networks connected to the
            pilot buses.
        name: str
            Name of this area, used mostly for debugging purposes and to
            interpret results.
        """

        # We store the system as an attribute in case we need to modify any of
        # its elements, e.g. when calling Area.get_load_increase_by_at().
        self.system = system

        # The specified strings are mapped to objects as these are easier to
        # work with. We also store the controlled classes as an attribute so
        # that they are accessible to update_pilot_DERs().
        self.controlled_classes = controlled_classes
        self.uncontrolled_load_buses: list[Bus] = [
            system.get_bus(name) for name in uncontrolled_load_bus_names
        ]
        self.pilot_buses: list[Bus] = [
            system.get_bus(name) for name in pilot_names
        ]
        self.Nc = len(self.pilot_buses)
        self.all_buses = self.pilot_buses + self.uncontrolled_load_buses

        # To measure Pc and Qc, and for other computations, it's useful to have
        # access to the (single) DERs and loads connected to each pilot bus. 
        self.pilot_DERs: dict[str, list[Injector]] = {
            pilot_bus.name: [] for pilot_bus in self.pilot_buses
        }
        self.update_pilot_DERs()
        self.pilot_loads: dict[str, list[RestorativeLoad]] = {}
        for pilot_bus in self.pilot_buses:
            self.pilot_loads[pilot_bus.name] = [
                inj
                for inj in system.bus_to_injectors[pilot_bus]
                if isinstance(inj, RestorativeLoad)
            ]
        # We also check that there's only one load at each pilot bus.
        for pilot_name, load_list in self.pilot_loads.items():
            assert (
                len(load_list) == 1
            ), f"There's more than one load at pilot bus {pilot_name}."

        # When doing linear algebra later on, it's useful to know the indices
        # of the (un)controlled buses.
        self.c_indices: list[int] = [
            self.system.buses.index(b) for b in self.pilot_buses
        ]
        self.u_indices: list[int] = [
            self.system.buses.index(b) for b in self.uncontrolled_load_buses
        ]

        # The following voltage limits only apply to the pilot buses, which are
        # the only ones being controlled.
        self.pilot_V_pu_min = pilot_V_pu_min
        self.pilot_V_pu_max = pilot_V_pu_max

        # Besides the voltage limits, the area also must be aware of the
        # capability curves that it must conform to.
        self.capability_curves = capability_curves

        self.name = name

        # Finally, we store the restorative loads from all buses
        self.uncontrolled_loads: dict[str, RestorativeLoad] = {}
        for bus in self.uncontrolled_load_buses:
            for inj in system.bus_to_injectors[bus]:
                if isinstance(inj, RestorativeLoad):
                    self.uncontrolled_loads[bus.name] = inj

        self.all_loads: list[RestorativeLoad] = []
        for bus in self.all_buses:
            for inj in system.bus_to_injectors[bus]:
                if isinstance(inj, RestorativeLoad):
                    self.all_loads.append(inj)

    def get_Qc_inj_Mvar_min_max(
        self,
        pilot_name: str,
        Pc_inj_MW: float,
        Vc_pu: float,
    ) -> tuple[float]:
        """Return min and max Q that the specified pilot bus can inject."""

        return self.capability_curves[pilot_name].get_Q_inj_Mvar_limits(
            P_inj_MW=Pc_inj_MW,
            V_pu=Vc_pu,
        )

    def update_pilot_DERs(self) -> None:
        """Update the DERs connected to the pilot bus."""

        for pilot_bus in self.pilot_buses:
            injector_list = []
            for inj in self.system.bus_to_injectors[pilot_bus]:
                if any(
                    isinstance(inj, controlled_class)
                    for controlled_class in self.controlled_classes
                ):
                    injector_list.append(inj)
            self.pilot_DERs[pilot_bus.name] = injector_list
        

    def get_load_increase_by_at(
        self, P_perc: float, Q_perc: float, ocurrence_time: float
    ) -> list[Disturbance]:
        """
        Return disturbances increase loads in this area by the same percentage.
        """

        dists = []

        for load in self.all_loads:

            dists += [
                # Change in P
                Disturbance(
                    ocurrence_time=ocurrence_time,
                    object_acted_on=load,
                    par_name="P0",
                    par_value=P_perc,
                    duration=0.0,
                    units="%",
                ),
                # Change in Q
                Disturbance(
                    ocurrence_time=ocurrence_time,
                    object_acted_on=load,
                    par_name="Q0",
                    par_value=Q_perc,
                    duration=0.0,
                    units="%",
                ),
            ]

        return dists

    def get_J(self) -> tuple[np.ndarray]:
        """
        Return area-specific Jacobian matrix used by the controller.

        This matrix, whose entries are of the form (partial Qi / partial Vj),
        assumes that Qi is being injected (not from the network, but by the
        injectors) into the bus.

        This method should only be called in the __init__ of the
        AreaController, as it makes no sense to extract these matrices for
        every call of the optimizer.
        """

        # For the moment, each area knows its exact J matrix, but eventually it
        # would report to the AreaController the estimated one.

        return self.get_exact_J()

    def get_exact_J(self) -> tuple[np.ndarray]:
        """Return exact Jacobian matrix of this area."""

        dS_dVm = self.system.build_dS_dV()[0]
        dQ_dVm = dS_dVm.imag

        # This fancy indexing is made possible by... numpy's ix_! Maybe check
        # out this function's documentation, as this way of indexing isn't
        # standard.

        Jcc = dQ_dVm[np.ix_(self.c_indices, self.c_indices)]
        Jcu = dQ_dVm[np.ix_(self.c_indices, self.u_indices)]
        Juc = dQ_dVm[np.ix_(self.u_indices, self.c_indices)]
        Juu = dQ_dVm[np.ix_(self.u_indices, self.u_indices)]

        # Here's a message for the future: instead of indexing, you might want
        # to do a full Kron reduction, keeping from the full J only those
        # entries associated to this area. My intuition is that this Kron
        # reduction would result in a sort of total derivative, while the
        # current indexing assumes that only one variable changes at the time;
        # furthermore, I think that the two procedures should give similar
        # results.

        # Update: After discussing with Georgia and Gustavo, we agreed to stick
        # to the indexing.

        return np.vstack(
            [
                np.hstack([Jcc, Jcu]),
                np.hstack([Juc, Juu]),
            ]
        )

class Event:
    """Anything that is happening in the power system or within a controller."""

    def __init__(
        self,
        t: float,
        agent: str,
        measurements: dict[str, tuple[float, str]],
        description: str,
        disturbances: list[Disturbance],
    ) -> None:
        """
        Create an event.

        Arguments
        ---------
        t: float
            Ocurrence time of the event (s).
        agent: str
            Entity responsible for the event, e.g. environment, controller.
        measurements: dict[str, tuple[float, str]]
            Measurements that are relevant to understand this event. 
        description: str
            Text description of this event.
        disturbances: list[Disturbance]
            Formal (i.e. in RAMSES' syntax) disturbances associated to this
            event. In the end, they'll be converted to text.
        """

        self.t = t
        self.agent = agent
        self.measurements = measurements
        self.description = description
        self.disturbances = disturbances

    def __lt__(self, other: "Event") -> bool:
        """Compare two events so that sequences of events can be sorted."""
        return self.t < other.t

    def __eq__(self, other: "Event") -> bool:
        return (
            self.t == other.t
            and
            self.agent == other.agent
            and
            self.description == other.description
            and 
            self.disturbances == other.disturbances
        )

    def get_rows(self) -> list[list[str | float]]:
        """
        Return rows describing this event.

        This method will be called for several events in order to display all
        of them in a table, built using the tabulate package.
        """

        rows = []

        for time, agen, meas, desc, dist in zip_longest(
            [self.t], 
            [self.agent],
            self.measurements, 
            [self.description], 
            self.disturbances,
        ):

            measured_value = None if meas is None else f"{self.measurements[meas][0]:.3f}"
            measured_units = None if meas is None else self.measurements[meas][1]
            time = None if time is None else f"{time:.3f}"

            rows.append(
                [
                    time, 
                    agen,
                    meas, 
                    measured_value,
                    measured_units, 
                    desc, 
                    dist if dist is None else str(dist),
                ]
            )

        return rows

class AreaController(Controller):
    """
    A voltage controller associated to each area.

    A system could be subject to many instances of this controller.
    """

    def __init__(
        self,
        area: Area,
        t_control_s: float,
        Qc_inj_Mvar_increment_limits: dict[str, tuple[float]],
        weight_slacks: np.ndarray,
        weight_Vu_deviations: float = 1.0,
        period_s: float = 0.2,
        V_tol_pu: float = 0.005,
    ):
        """
        Create an area controller.

        Arguments
        ---------
        area: Area
            Area this controller is in charge of.
        t_control_s: float
            Absolute time at which the controller starts acting (s), which is
            typically 30 s after the disturbance, so that the controller first
            lets the primary voltage control act.
        Qc_inj_Mvar_increment_limits: dict[str, tuple[float]]
            For each pilot bus, minimum and maximum values allowed for
            increment_Qc (Mvar), i.e. the change of reactive power at the pilot
            bus per iteration of the controller.
        weight_slacks: np.ndarray
            Matrix W, i.e. 2-D array, which penalizes the slack variables
            s in the objective function through a term of the form s.T @ W @ s.
        weight_Vu_deviations: float
            Weight of the term that includes the infinity norm; see paper.
        period_s: float
            Time difference between two consecutive actions of this controller
            (s), which is typically 0.2 s but might change depending on the
            speed of the DERs.
        V_tol_pu: float
            Tolerance within which the voltage deviations will NOT trigger the
            area controller.
        """

        # We first initialize the system governed by the controller. This
        # attribute is then overwritten by System's add_controllers() method.
        self.sys: "System" = None

        # We then initialize the other attributes that are common to all
        # controllers. The RAMSES interface needs them in order to ask the
        # controller at the right times for control actions.
        self.t_last_action: float = 0
        self.period: float = period_s

        # The following attributes are specific to this controller.
        self.area = area
        self.t_control_s = t_control_s
        self.Qc_inj_Mvar_increment_limits = Qc_inj_Mvar_increment_limits
        self.weight_slacks = weight_slacks
        self.weight_Vu_deviations = weight_Vu_deviations
        self.V_tol_pu = V_tol_pu
        self.J = area.get_J()

        assert weight_slacks.shape == (2*area.Nc, 2*area.Nc), (
            "Weight matrix of slack variables has the wrong shape: "
            f"I expected {(2*area.Nc, 2*area.Nc)}, "
            f"but got {weight_slacks.shape}."
        )

        # Some initial values we only compute once.
        self.V_pu_0 = np.array([[b.V_pu] for b in self.area.all_buses])
        self.Q_inj_pu_0 = np.array(
            [
                [
                    sum(inj.get_Q() for inj in self.area.system.bus_to_injectors[bus])
                    for bus in self.area.all_buses
                ]
            ]
        )
        self.Vc_0 = self.V_pu_0[: self.area.Nc, 0]  # is 1-D
        self.Qc_inj_pu_0 = self.Q_inj_pu_0[: self.area.Nc, 0]  # is 1-D

        # Some bounds for constraints we also compute only once.
        increment_Qc_inj_pu_min = []
        increment_Qc_inj_pu_max = []
        for pilot_bus in self.area.pilot_buses:
            # The decision variable is in the system's base, and so must be the
            # bounds.
            increment_Qc_inj_pu_min.append(
                self.Qc_inj_Mvar_increment_limits[pilot_bus.name][0]
                / self.area.system.base_MVA
            )
            increment_Qc_inj_pu_max.append(
                self.Qc_inj_Mvar_increment_limits[pilot_bus.name][1]
                / self.area.system.base_MVA
            )
        self.increment_Qc_inj_pu_min = np.array(increment_Qc_inj_pu_min)
        self.increment_Qc_inj_pu_max = np.array(increment_Qc_inj_pu_max)

        self.name = f"Controller at area {self.area.name}"

        # Finally, to interpret the decisions of this controller, we keep track
        # of all the events related to it. 
        self.events: list[Event] = []

    def log_event(
        self, 
        description: str, 
        disturbances: list[Disturbance],
        t: float = None,
        measurements: dict[str, tuple[float, str]] = {}, 
        agent: str = "",
    ) -> None:
        """Record event associated to this controller."""

        event = Event(
            t=self.sys.get_t_now() if t is None else t,
            agent=agent if agent else self.name,
            measurements=measurements,
            description=description,
            disturbances=disturbances,
        )

        insort(self.events, event)

    def write_log(self, filename: str) -> None:
        """Write a log with the events associated to this controller."""
        
        write_log(events=self.events, filename=filename)

    def get_V_pu_now_of(self, bus: Bus) -> float:
        """Return voltage magnitude (right now) of a single bus."""

        return self.sys.ram.getBusVolt(busNames=[bus.name])[0]

    def get_V_pu_now(self) -> np.ndarray:
        """Return [Vc^T, Vu^T]^T, where V = V(now)."""

        return np.array([[self.get_V_pu_now_of(bus=b)] for b in self.area.all_buses])

    def get_delta_V_pu_now(self) -> np.ndarray:
        """Return [dVc^T, dVu^T]^T, where dV = V(now) - V(0)."""

        return self.get_V_pu_now() - self.V_pu_0

    def get_delta_Qu_inj_pu_now(self, delta_V_pu_now: np.ndarray) -> np.ndarray:
        """
        Return deviations of reactive power.

        Instead of being measured, these deviations are approximated using eq.
        (21) of the paper. Because of the units assumed by System, the J
        matrices must be multiplied by pu voltages in order to give pu reactive
        powers.

        Possible extension: make this a measurement, instead of an estimation.
        """

        # Whatever is after the first Nc elements, are the elements associated
        # to uncontrolled buses.
        return (self.J @ delta_V_pu_now)[self.area.Nc :, :]

    def get_measured_delta_Qu_inj_pu_now(self) -> np.ndarray:
        """Return measured deviations of reactive power from initial values."""

        Qu_inj_pu_0 = np.array(
            self.sys.get_S_towards_network()
        ).imag[self.area.u_indices, 0]
        
        Qu_inj_pu_now = self.get_Pu_Qu_inj_pu_now()[1]
        
        return Qu_inj_pu_now - Qu_inj_pu_0

    def get_Pu_Qu_inj_pu_now(self) -> np.ndarray:
        """Return active and reactive power at the uncontrolled buses."""

        Pu_inj_pu_now = []
        Qu_inj_pu_now = []

        for uncontrolled_bus in self.area.uncontrolled_load_buses:

            load = self.area.uncontrolled_loads[uncontrolled_bus.name]

            # Power consumed by the load, in the system's base

            P_LOAD_dem_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                comp_name=[load.name],
                obs_name=["P"],
            )[
                0
            ]  # extract only element of the list

            Q_LOAD_dem_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                comp_name=[load.name],
                obs_name=["Q"],
            )[
                0
            ]  # extract only element of the list


            Pu_inj_pu_now.append(- P_LOAD_dem_pu)
            Qu_inj_pu_now.append(- Q_LOAD_dem_pu)

        return (
            np.array(Pu_inj_pu_now),
            np.array(Qu_inj_pu_now),
        )

    def get_Pc_Qc_inj_pu_now(self) -> np.ndarray:
        """Return active and reactive power at the pilot buses."""

        Pc_inj_pu_now = []
        Qc_inj_pu_now = []

        for pilot_bus in self.area.pilot_buses:

            # Notice that we had already checked that the lists only contain
            # one element.
            load = self.area.pilot_loads[pilot_bus.name][0]
            DER = self.area.pilot_DERs[pilot_bus.name][0]

            # Power consumed by the load, in the system's base

            P_LOAD_dem_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                comp_name=[load.name],
                obs_name=["P"],
            )[
                0
            ]  # extract only element of the list

            Q_LOAD_dem_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                comp_name=[load.name],
                obs_name=["Q"],
            )[
                0
            ]  # extract only element of the list

            # Power injected by the DER, in the DER's base

            P_DER_inj_own_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                # Adjust if more injectors are controlled at the pilot bus
                comp_name=[DER.name],
                obs_name=["Pgen"],
            )[
                0
            ]  # extract only element of the list

            Q_DER_inj_own_pu = self.sys.ram.getObs(
                comp_type=["INJ"],
                # Adjust if more injectors are controlled at the pilot bus
                comp_name=[DER.name],
                obs_name=["Qgen"],
            )[
                0
            ]  # extract only element of the list

            P_DER_inj_pu = P_DER_inj_own_pu * DER.Snom_MVA / self.area.system.base_MVA
            Q_DER_inj_pu = Q_DER_inj_own_pu * DER.Snom_MVA / self.area.system.base_MVA

            # The Jacobian matrix was built assuming that the powers are injected
            # into the bus by injectors (and also that they are in pu in the
            # system's base). Furthermore, the 'P' (resp. 'Q') observable of the
            # RESTLOAD assumes that the power is entering the load, while the
            # 'Pgen' (resp. 'Qgen') observable of the DERA assumes that the power
            # is exiting the DER.

            Pc_inj_pu_now.append(P_DER_inj_pu - P_LOAD_dem_pu)
            Qc_inj_pu_now.append(Q_DER_inj_pu - Q_LOAD_dem_pu)

        return (
            np.array(Pc_inj_pu_now),
            np.array(Qc_inj_pu_now),
        )

    def solve_optimization(
        self,
        V_pu_now: np.ndarray,  # 2D, to make array operations easier
        delta_V_pu_now: np.ndarray,  # 2D, to make array operations easier
        delta_Qu_inj_pu_now: np.ndarray,  # 2D, to make array operations easier
        Pc_inj_pu_now: np.ndarray,  # 1D, to embed it directly into constraints
        Qc_inj_pu_now: np.ndarray,  # 1D, to embed it directly into constraints
    ) -> float:
        """
        Solve the optimization problem that underpins this controller.

        We use scipy for the first implementation, but this should be migrated
        to gurobi. Furthermore, we implement some of the constraints as
        NonlinearConstraints, even though they're linear. This is just because
        the implementation is easier to change later on, should we change our
        mind on the formulation. This shouldn't be a big problem since the
        scale of the problem is small, but still, this should all be formulated
        in linear form; this probably would help the solver.
        """

        # Since the matrices that go into the objective function and the
        # constraints are time-varying, we're forced to define those functions
        # inside this method, even though this might look inefficient.

        # For the sake of consistency, we'll work with 1D arrays, meaning that
        # we won't distinguish between column and row vectors, or meaning,
        # equivalently, that x.shape == (N,) for some N. In fact, if
        # self.area.Nc is the number of pilot buses int his area, then N = Nc +
        # 2*Nc, where the second term accounts for the slack variables.

        # In all the definitions that follow, x always stands for the full, 1D
        # vector of decision variables; all other variations, either with
        # another shape or size, are given other names.

        # For the initial guess, it makes sense to assume dQc = 0 and null
        # slack variables.
        x0 = np.zeros(
            self.area.Nc  # reactive power change at the pilot buses
            + 2
            * self.area.Nc  # slack variables for voltage at the pilot buses
        )

        # The voltage deviations appear a few times, so it's useful to have a
        # local function just for that:

        def get_delta_V_next(x: np.ndarray) -> np.ndarray:
            """Return prediction of voltage deviations."""

            x_col = x[: self.area.Nc].reshape((-1, 1))

            return (
                np.linalg.inv(self.J)
                @
                # The following vector is delta_Q_pu_next
                np.vstack(
                    [
                        x_col,  # delta_Qc_pu_next
                        delta_Qu_inj_pu_now,  # approximately delta_Qu_pu_next
                    ]
                )
            )  # Returns a 2D array

        # Objective function

        def get_Vu_deviations(x: np.ndarray) -> np.ndarray:
            """
            Return an interpretation of the deviations of uncontrolled voltages.
            """

            return get_delta_V_next(x=x)[self.area.Nc :, :]

        def objective(x: np.ndarray) -> float:
            """
            Penalize deviations of uncontrolled voltages, and slack variables.
            """

            slacks = x[self.area.Nc :].reshape((-1, 1))

            return (
                self.weight_Vu_deviations
                * np.linalg.norm(
                    # The vector we're taking the norm of contains the voltages
                    # of the uncontrolled buses. Even if this vector is 2D, the
                    # function still returns a scalar.
                    get_Vu_deviations(x=x),
                    np.inf,
                )
                + (slacks.T @ self.weight_slacks @ slacks)[
                    0, 0
                ]  # turn into a scalar
            )

        # Constraint #1: reactive power at pilot bus. Notice that these bounds
        # are dynamic, since they're read from the capability curves, and hence
        # they have to be reconstructed during every call to the optimizer.

        Qc_inj_Mvar_min = []
        Qc_inj_Mvar_max = []
        for i, pilot_bus in enumerate(self.area.pilot_buses):
            Q_min, Q_max = self.area.get_Qc_inj_Mvar_min_max(
                pilot_name=pilot_bus.name,
                Pc_inj_MW=Pc_inj_pu_now[i],  # remember this is 1-D
                Vc_pu=V_pu_now[i, 0],
            )
            Qc_inj_Mvar_min.append(Q_min)
            Qc_inj_Mvar_max.append(Q_max)


        def get_Qc_inj_pu_next(x: np.ndarray) -> np.ndarray:
            """Get predicted Qc from the next iteration."""

            delta_Qc_inj_pu_next = x[: self.area.Nc]

            return self.Qc_inj_pu_0 + delta_Qc_inj_pu_next

        constraint_Qc_inj = NonlinearConstraint(
            fun=get_Qc_inj_pu_next,
            # The following bounds are converted to the system's base:
            lb=np.array(Qc_inj_Mvar_min) / self.area.system.base_MVA,
            ub=np.array(Qc_inj_Mvar_max) / self.area.system.base_MVA,
        )

        # Constraint #2: voltage at pilot bus

        V_pu_min = []
        V_pu_max = []
        for pilot_bus in self.area.pilot_buses:
            V_pu_min.append(self.area.pilot_V_pu_min[pilot_bus.name])
            V_pu_max.append(self.area.pilot_V_pu_max[pilot_bus.name])

        # Lower constraint

        def get_V_relaxed_below(x: np.ndarray) -> np.ndarray:
            """Return an auxiliary quantity."""

            delta_Vc_next = get_delta_V_next(x=x)[: self.area.Nc, 0]
            Vc_next = self.Vc_0 + delta_Vc_next

            s1 = x[self.area.Nc : 2 * self.area.Nc]

            return -s1 - Vc_next

        constraint_Vc_below = NonlinearConstraint(
            fun=get_V_relaxed_below,
            lb=np.full(self.area.Nc, -np.inf),  # unbounded below
            ub=-np.array(V_pu_min),  # yes, the -1 is intended to be there
        )

        # Upper constraint
        def get_V_relaxed_above(x: np.ndarray) -> np.ndarray:
            """Return another auxiliary quantity."""

            delta_Vc_next = get_delta_V_next(x=x)[: self.area.Nc, 0]
            Vc_next = self.Vc_0 + delta_Vc_next

            s2 = x[2 * self.area.Nc : 3 * self.area.Nc]

            return -s2 + Vc_next

        constraint_Vc_above = NonlinearConstraint(
            fun=get_V_relaxed_above,
            lb=np.full(self.area.Nc, -np.inf),  # unbounded below
            ub=np.array(V_pu_max),
        )

        # Constraint #4: changes of reactive power at pilot bus

        bounds = Bounds(
            lb=np.hstack(
                [
                    (
                        self.increment_Qc_inj_pu_min
                        - (self.Qc_inj_pu_0 - Qc_inj_pu_now)
                    ),
                    np.zeros(2 * self.area.Nc),
                ]
            ),
            ub=np.hstack(
                [
                    (
                        self.increment_Qc_inj_pu_max
                        - (self.Qc_inj_pu_0 - Qc_inj_pu_now)
                    ),
                    np.full(2 * self.area.Nc, np.inf),
                ]
            ),
        )

        # Solution
        result = minimize(
            fun=objective,
            x0=x0,
            bounds=bounds,
            constraints=[constraint_Qc_inj, constraint_Vc_below, constraint_Vc_above],
            # method="SLSQP",
            method="Powell",
            # options={"disp": True},#, "maxiter": 100},
        )

        # Log some results about the optimization
        self.log_event(
            description="Finished solving optimization",
            disturbances=[],
            measurements=(
                # Status
                {"success (1 if successful)": (True, None)}
                |
                # Objective function
                {"objective function": (result.fun, None)}
                |
                # Upper slack variables in pu
                {
                    f"upper slack of bus {b.name}": (s, "pu") 
                    for b, s in zip(
                        self.area.pilot_buses, 
                        result.x[self.area.Nc:2*self.area.Nc],
                    )
                }
                |
                # Lower slack variables in pu
                {
                    f"lower slack of bus {b.name}": (s, "pu") 
                    for b, s in zip(
                        self.area.pilot_buses, 
                        result.x[2*self.area.Nc:3*self.area.Nc],
                    )
                }
            ),
        )

        # Remember that we need to return a 1D array
        return result.x[: self.area.Nc]

    def get_DER_commands(
        self, delta_Qc_inj_pu_optimal: np.ndarray
    ) -> list[Disturbance]:
        """
        Get "disturbances" that will be sent to the controlled DER.
        """

        dists = []

        for pilot_bus, delta_Qc_inj_pu_optimal_individual in zip(
            self.area.pilot_buses, 
            delta_Qc_inj_pu_optimal,
        ):

            # We first fetch the DER that will be acted on. This list only
            # has one element.
            DER = self.area.pilot_DERs[pilot_bus.name][0]

            Q_inj_ref_pu_system_base = (
                DER.get_Q() / self.sys.base_MVA
                +
                delta_Qc_inj_pu_optimal_individual
            )

            Q_inj_ref_pu_DER_base = (
                Q_inj_ref_pu_system_base
                *
                self.sys.base_MVA
                /
                DER.Snom_MVA
            )

            # The following is a transmission delay, which I think (intuitively)
            # that should be at least larger than RAMSES' integration step, and
            # definitely smaller than this controller's period.
            DELAY_s = 0.02  # 10 times the int. step, half of the period
            dist = Disturbance(
                ocurrence_time=self.sys.get_t_now() + DELAY_s,
                object_acted_on=DER,
                par_name="Qref",
                # Remember that delta_Qref_pu is in the system's base, but this
                # needs to be converted to the DER's base.
                par_value=Q_inj_ref_pu_DER_base,
            )

            dists.append(dist)

        return dists

    def print_this(self, message: str) -> None:

        print(f"\n{self.name} at t = {self.sys.get_t_now():.1f} s: {message}")

    def get_actions(self) -> list[Disturbance]:
        """Return the actions to be taken by the controller."""

        # We first need to let the primary control act (mostly AVRs). We don't
        # send any control actions initially.
        if self.sys.get_t_now() <= self.t_control_s:
            self.log_event(
                description="Letting primary control act",
                disturbances=[],
            )
            return []

        # The optimization also needs to know the present voltage, to know which
        # limits to read from the capability curve.
        V_pu_now = self.get_V_pu_now()
        self.log_event(
            description="Measuring V",
            disturbances=[],
            measurements={
                f"V at bus {bus.name} ({'c' if bus in self.area.pilot_buses else 'u'})": (V, "pu") 
                for bus, V in zip(self.area.all_buses, V_pu_now[:, 0])
            },
        )

        # If the primary control is over, we still check if the voltage
        # deviations are significant. If not, we don't send any control
        # actions.
        delta_V_pu_now = self.get_delta_V_pu_now()
        self.log_event(
            description="Measuring delta_V",
            disturbances=[],
            measurements={
                f"delta_V at bus {bus.name} ({'c' if bus in self.area.pilot_buses else 'u'})": (dV, "pu") 
                for bus, dV in zip(self.area.all_buses, delta_V_pu_now[:, 0])
            },
        )
        if np.max(np.abs(delta_V_pu_now)) < self.V_tol_pu:
            self.log_event(
                description="Remaining iddle due to negligible deviations",
                disturbances=[],
            )
            return []

        # If the voltage deviations are significant, we estimate the deviations
        # of reactive power.
        delta_Qu_inj_pu_now = self.get_delta_Qu_inj_pu_now(delta_V_pu_now)
        delta_Qu_inj_pu_now_measured = self.get_measured_delta_Qu_inj_pu_now()
        self.log_event(
            description="Measuring delta_Qu_inj",
            disturbances=[],
            measurements={
                f"delta_Qu_inj at bus {bus.name}": (dQu*self.sys.base_MVA, "Mvar") 
                for bus, dQu in zip(
                    self.area.uncontrolled_load_buses, 
                    delta_Qu_inj_pu_now_measured,
                )
            },
        )

        # We'll keep only the measured values.
        delta_Qu_inj_pu_now = delta_Qu_inj_pu_now_measured.reshape((-1, 1))

        # The only measurement that is missing is that of Qc, the injected
        # reactive power at the pilot bus. Remember that Q should be treated in
        # pu in the system's base.
        Pc_inj_pu_now, Qc_inj_pu_now = self.get_Pc_Qc_inj_pu_now()
        self.log_event(
            description="Measuring Pc_inj",
            disturbances=[],
            measurements={
                f"Pc_inj at bus {bus.name}": (Pc*self.sys.base_MVA, "MW") 
                for bus, Pc in zip(
                    self.area.pilot_buses,
                    Pc_inj_pu_now,
                )
            },
        )
        self.log_event(
            description="Measuring Qc_inj",
            disturbances=[],
            measurements={
                f"Qc_inj at bus {bus.name}": (Qc*self.sys.base_MVA, "Mvar") 
                for bus, Qc in zip(
                    self.area.pilot_buses,
                    Qc_inj_pu_now,
                )
            },
        )

        # We're ready to solve the optimization:
        delta_Qc_inj_pu_optimal = self.solve_optimization(
            V_pu_now=V_pu_now,
            delta_V_pu_now=delta_V_pu_now,
            delta_Qu_inj_pu_now=delta_Qu_inj_pu_now,
            Pc_inj_pu_now=Pc_inj_pu_now,
            Qc_inj_pu_now=Qc_inj_pu_now,
        )
        self.log_event(
            description="Obtaining optimal change in Qc",
            disturbances=[],
            measurements={
                f"delta_Qc_inj at bus {bus.name}": (delta_Qc*self.sys.base_MVA, "Mvar") 
                for bus, delta_Qc in zip(
                    self.area.pilot_buses,
                    delta_Qc_inj_pu_optimal,
                )
            },
        )

        dists = self.get_DER_commands(delta_Qc_inj_pu_optimal=delta_Qc_inj_pu_optimal)
        self.log_event(
            description="Sending commands to the DERs",
            disturbances=dists,
        )

        return dists


class DERPowers(Visualization):
    """Powers of all loads against time."""

    name = "DER_powers"

    def generate(
        self, system: System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        plt.figure()

        for inj in system.injectors:
            if isinstance(inj, DERA):

                # Extract data
                data = extractor.getInj(inj.name)
                P_old = data.Pgen.value
                Q_old = data.Qgen.value
                t_old = data.Qgen.time

                # Resample data
                step = 0.02
                t = np.arange(t_old[0], t_old[-1] + step, step)
                P = np.interp(t, t_old, P_old)
                Q = np.interp(t, t_old, Q_old)

                # Add plots
                plt.plot(
                    t,
                    P * inj.Snom_MVA,
                    label=f"P {inj.name}",
                )
                plt.plot(
                    t,
                    Q * inj.Snom_MVA,
                    label=f"Q {inj.name}",
                )

        plt.title("Powers injected by the DERs")
        plt.xlabel("Time (s)")
        plt.ylabel("Active/reactive powers (MW/Mvar)")
        plt.legend()
        self.save_figure(vis_dir=vis_dir, filename="der_powers.pdf")


class VoltageLambda(Metric):
    """
    Compute the root mean square value of the voltage deviations at the
    voltage-uncontrolled load buses in steady state.

    See eq. (35) of Pierrou's paper.

    If the purpose is to return to pre-disturbance values, this metric would
    ideally be zero.
    """

    units = "pu"

    def __init__(self, area: Area) -> None:

        self.area = area
        self.name = f"lambda of {area.name}; see eq. (35) of paper"

    def evaluate(self, system: System, extractor: pyramses.extractor) -> float:

        def get_delta_V_pu_ss(bus: Bus) -> float:
            """Get deviation with respect to the steady-state voltages."""

            data = extractor.getBus(bus.name)
            voltage = data.mag.value

            return voltage[-1] - voltage[0]

        N_u = len(self.area.uncontrolled_load_buses)
        norm_squared = 0
        for bus in self.area.uncontrolled_load_buses:
            norm_squared += get_delta_V_pu_ss(bus=bus) ** 2

        return np.sqrt(norm_squared / N_u)


class VoltageViolation(Metric):
    """
    Compute the violation of the voltage constraints at the pilot bus.

    Ideally, this metric would be zero.
    """

    units = "pu"

    def __init__(self, area: Area) -> None:

        self.area = area
        self.name = (
            f"either 0 or abs. of (V_pilot - violated_limit), for {area.name}"
        )

    def evaluate(self, system: System, extractor: pyramses.extractor) -> float:

        data = extractor.getBus(self.area.pilot_bus.name)
        voltage = data.mag.value

        # If there's an undervoltage:
        if voltage[-1] < self.area.pilot_V_pu_min:
            violation = np.abs(voltage[-1] - self.area.pilot_V_pu_min)

        # If, instead, there's an overvoltage:
        elif voltage[-1] > self.area.pilot_V_pu_max:
            violation = np.abs(voltage[-1] - self.area.pilot_V_pu_max)

        # If we got here, it's because there was no violation:
        else:
            violation = 0

        return violation


def get_experiment(horizon: float) -> tuple[Experiment, System]:
    """
    Return the main experiment for the IREP'2025 paper.

    After applying the following disturbances,

        - at t = 5 s, a 50% increase of all loads (P and Q) in area 3,

    we simulate the system

        - when only the controller in area 3 acts and
        - when the controllers in all areas (1-4) act,

    where each controller, as in the paper, solves an optimization problem that
    is specific to its area.

    The visualizations include

        - voltage magnitudes (pu) against time (s) at all buses,
        - load powers (MW) against time (s) at pilot buses,
        - DER powers (Mvar) against time (s) at pilot buses,

    whereas the performance metrics include

        - performance index lambda from eq. 35 of the paper,

    """

    exp = Experiment(
        name="Prototype",
        DLL_dir=(
            r"C:\Users\fescobar\Downloads"
            #r"H:\RAMSES\URAMSES-3.40c\URAMSES-3.40c" r"\Release_intel_w64"
        ),
    )

    system = get_IEEE_39()

    # Because of the way I programmed the interface, the controllers won't
    # become active unless there is a contingency, i.e. an outage. To
    # circumvent this, we make the system believe that there is such a
    # contingency from the very beginning of the simulation, while in reality
    # there is none.
    system.force_contingency()

    # Before defining the controllers, we need to define the four areas. As
    # documented in the Area class, we only need to specify the load buses
    # (uncontrolled and pilot), which is the reason why we don't list the whole
    # 39 buses.

    # The following definitions might seem redundant, but they're actually
    # useful for defining the wide-area control.

    # Eventually, the voltage limits should be dependent on the initial power
    # flows.

    u_names_1 = ["1", "18"]
    p_names_1 = ["3"]
    V_min_1 = {"3": 0.94}
    V_max_1 = {"3": 1.07}
    curves_1 = {"3": ExampleCurve()}

    u_names_2 = ["4", "7", "9", "12"]
    p_names_2 = ["8"]
    V_min_2 = {"8": 0.93}
    V_max_2 = {"8": 1.005}
    curves_2 = {"8": ExampleCurve()}

    u_names_3 = ["15", "20", "21", "23", "24"]
    p_names_3 = ["16"]
    V_min_3 = {"16": 0.98}
    V_max_3 = {"16": 1.045}
    curves_3 = {"16": ExampleCurve()}

    u_names_4 = ["25", "27", "28", "29"]
    p_names_4 = ["26"]
    V_min_4 = {"26": 0.94}
    V_max_4 = {"26": 1.06}
    curves_4 = {"26": ExampleCurve()}

    area_1 = Area(
        system=system,
        uncontrolled_load_bus_names=u_names_1,
        pilot_names=p_names_1,
        controlled_classes=[DERA],
        pilot_V_pu_min=V_min_1,
        pilot_V_pu_max=V_max_1,
        capability_curves=curves_1,
        name="A1",
    )

    area_2 = Area(
        system=system,
        uncontrolled_load_bus_names=u_names_2,
        pilot_names=p_names_2,
        controlled_classes=[DERA],
        pilot_V_pu_min=V_min_2,
        pilot_V_pu_max=V_max_2,
        capability_curves=curves_2,
        name="A2",
    )

    area_3 = Area(
        system=system,
        uncontrolled_load_bus_names=u_names_3,
        pilot_names=p_names_3,
        controlled_classes=[DERA],
        pilot_V_pu_min=V_min_3,
        pilot_V_pu_max=V_max_3,
        capability_curves=curves_3,
        name="A3",
    )

    area_4 = Area(
        system=system,
        uncontrolled_load_bus_names=u_names_4,
        pilot_names=p_names_4,
        controlled_classes=[DERA],
        pilot_V_pu_min=V_min_4,
        pilot_V_pu_max=V_max_4,
        capability_curves=curves_4,
        name="A4",
    )

    area_super = Area(
        system=system,
        uncontrolled_load_bus_names=(
            u_names_1 + u_names_2 + u_names_3 + u_names_4
        ),
        pilot_names=(p_names_1 + p_names_2 + p_names_3 + p_names_4),
        controlled_classes=[DERA],
        pilot_V_pu_min=(V_min_1 | V_min_2 | V_min_3 | V_min_4),
        pilot_V_pu_max=(V_max_1 | V_max_2 | V_max_3 | V_max_4),
        capability_curves=(curves_1 | curves_2 | curves_3 | curves_4),
        name="AS",
    )

    areas = [area_1, area_2, area_3, area_4]

    # At the pilot bus of each area, we add a DERA with null active power and
    # whose capacity is half of the load.

    def install_DERA(area: Area) -> None:
        """Connect a DER_A at each pilot bus of the specified area."""

        for pilot_bus in area.pilot_buses:

            pilot_load = next(
                inj
                for inj in system.bus_to_injectors[pilot_bus]
                if isinstance(inj, RestorativeLoad)
            )

            der = DERA(
                name=f"DERA_{pilot_bus.name}",
                bus=pilot_bus,
                P0_MW=0,
                Q0_Mvar=1e-9,  # nonzero so that they are printed to the RAMSES file
                Snom_MVA=0.5 * pilot_load.P0_MW,
            )
            system.store_injector(der)
            # area.update_pilot_DERs()

    for area in areas:
        install_DERA(area)

    # We update the pilot injectors:
    for area in [area_super] + areas:
        area.update_pilot_DERs()

    # Finally, we check that there's only one DER per pilot bus.
    for area in [area_super] + areas:
        for pilot_name, DER_list in area.pilot_DERs.items():
            assert (
                len(DER_list) == 1
            ), f"There's more than one DER at pilot bus {pilot_name}."

    # Now that we've fully defined the system, we add it to the experiment.
    # IMPORTANT: changes to the system must be made before this line!
    exp.add_system("IEEE 39", system)

    # Next, we define the disturbances. We change the loads in area 3 by 50% of
    # their initial value, which should mean that the total load of the system
    # is increasing by about 20%.
    T_DISTURBANCE_s = 5
    dists = area_3.get_load_increase_by_at(
        P_perc=50,
        Q_perc=50,
        ocurrence_time=T_DISTURBANCE_s,
    )
    exp.add_disturbances(
        f"Load A3",
        # The previous method returns a list (one disturbance per load), so we
        # need to unpack it before passing the disturbances to
        # add_disturbances.
        *dists,
    )

    # We add an area controller per area, and they all start acting 30 s after
    # the disturbance.
    DELAY_s = 30

    # Again, the following definitions might seem redundant, but they're useful
    # for defining the wide-area control.
    # example_limits = (-np.inf, np.inf)
    example_limits = (-0.1, 0.1)
    limits_1 = {b.name: example_limits for b in area_1.pilot_buses}
    limits_2 = {b.name: example_limits for b in area_2.pilot_buses}
    limits_3 = {b.name: example_limits for b in area_3.pilot_buses}
    limits_4 = {b.name: example_limits for b in area_4.pilot_buses}
    
    REL_WEIGHT_SLACKS = 1_00

    C1 = AreaController(
        area=area_1,
        t_control_s=T_DISTURBANCE_s + DELAY_s,
        Qc_inj_Mvar_increment_limits=limits_1,
        weight_slacks=np.diag(2*area_1.Nc*[REL_WEIGHT_SLACKS]),
    )
    C2 = AreaController(
        area=area_2,
        t_control_s=T_DISTURBANCE_s + DELAY_s,
        Qc_inj_Mvar_increment_limits=limits_2,
        weight_slacks=np.diag(2*area_2.Nc*[REL_WEIGHT_SLACKS]),
    )
    C3 = AreaController(
        area=area_3,
        t_control_s=T_DISTURBANCE_s + DELAY_s,
        Qc_inj_Mvar_increment_limits=limits_3,
        weight_slacks=np.diag(2*area_3.Nc*[REL_WEIGHT_SLACKS]),
    )
    C4 = AreaController(
        area=area_4,
        t_control_s=T_DISTURBANCE_s + DELAY_s,
        Qc_inj_Mvar_increment_limits=limits_4,
        weight_slacks=np.diag(2*area_4.Nc*[REL_WEIGHT_SLACKS]),
    )
    CS = AreaController(
        area=area_super,
        t_control_s=T_DISTURBANCE_s + DELAY_s,
        Qc_inj_Mvar_increment_limits=(
            limits_1 | limits_2 | limits_3 | limits_4
        ),
        weight_slacks=np.diag(2*area_super.Nc*[REL_WEIGHT_SLACKS]),
    )

    for dist in dists:
        for C in [C1, C2, C3, C4, CS]:
            C.log_event(
                t=dist.ocurrence_time,
                description="Load increase",
                disturbances=[dist],
                agent="Environment",
            )

    # Initially, only C3 acts, while in other run of the experiment we want all
    # of them simultaneously.
    exp.add_controllers("Only A3", C3)
    # exp.add_controllers("All areas", C1, C2, C3, C4)
    # exp.add_controllers("Super area", CS)

    # We add the visualizations
    exp.add_visualizations(
        Voltages(),
        LoadPowers(),
        DERPowers(),
    )

    # We also add the metrics
    exp.add_metrics(
        VoltageLambda(area=area_1),
        VoltageLambda(area=area_2),
        VoltageLambda(area=area_3),
        VoltageLambda(area=area_4),
        VoltageLambda(area=area_super),
        #VoltageViolation(area=area_1),
        #VoltageViolation(area=area_2),
        #VoltageViolation(area=area_3),
        #VoltageViolation(area=area_4),
        #VoltageViolation(area=area_super),
    )

    # Finally, we define some formalities for simulations in RAMSES.
    exp.set_solver_and_horizon(
        solver_settings_dict={
            "max_h": 0.02 / 2,  # to allow disturbances every 0.02 s = 20 ms
            "min_h": 0.001,
        },
        horizon=horizon,
    )
    exp.set_RAMSES_settings(
        settings_dict={"SPARSE_SOLVER": "ma41", "NB_THREADS": 10},
    )
    for element in system.buses + system.injectors + system.generators:
        exp.add_observables(Observable(element, "all"))
    exp.disturbance_window = (
        10  # undervoltages beyond this window will stop the simulation
    )

    # We're done defining the experiment, so good luck!
    return exp, system


if __name__ == "__main__":

    exp, system = get_experiment(horizon=40)

    with Timer("Running the WAVC"):
        exp.run(
            remove_trj=False,
            simulate_no_control=False,
            simulate_no_dist=False,
        )
