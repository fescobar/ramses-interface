"""
This file specifies simulations for an extension of 

    G. Pierrou, H. Lai, G. Hug, and X. Wang, ‘A decentralized wide-area voltage
    control scheme for coordinated secondary voltage regulation using PMUs’,
    IEEE Trans. Power Syst., pp. 1–13, 2024, doi: 10.1109/TPWRS.2024.3381553.

Specifically, this file deals with the generation of noisy data for the
estimation step. The control itself is implemented in another file
(control.py).

This program relies on functions and classes located in the directory
../../src. These functions and classes handle all aspects of the simulations,
from initializing the power flow cases to processing the results.

Francisco Escobar (fescobar@ethz.ch)
July 2024, Zurich
"""

# Packages from the standard library
import sys
import os
from dataclasses import dataclass
from typing import ClassVar

# Modules from this repository
sys.path.append(os.path.join("..", "..", "src"))
import pf_dynamic as pf
from records import Bus, Injector, Load, Parameter
from experiment import Experiment
from sim_interaction import Disturbance, Observable
from utils import Timer, reduce
from visual import Visualization

# Third-party packages
import numpy as np
import pyramses
import matplotlib.pyplot as plt
import scipy.io

# This program requires using restorative loads, and for including them into
# the system we need a child class of Injector.


@dataclass
class RestorativeLoad(Injector):
    """A restorative load restores its P and Q with time; see p. 128 onwards of
    Van Cutsem & Vournas (1998)."""

    bus: Bus
    FP: float
    FQ: float
    P0_MW: float
    Q0_Mvar: float
    Kpf: float
    alphat: float
    alphas: float
    zpmin: float
    zpmax: float
    Kqf: float
    betat: float
    betas: float
    zqmin: float
    zqmax: float
    Tpq: float
    prefix: ClassVar[str] = "INJEC RESTLD"

    def __post_init__(self) -> None:
        self.name = f"L_{self.bus.name}"

    def get_P(self) -> float:
        return -self.P0_MW

    def get_Q(self) -> float:
        return -self.Q0_Mvar

    def get_pars(self) -> list[Parameter]:

        return [
            Parameter("bus", self.bus.name),
            Parameter("FP", self.FP),
            Parameter("FQ", self.FQ),
            Parameter("P", 0),
            Parameter("Q", 0),
            Parameter("Kpf", self.Kpf),
            Parameter("alphat", self.alphat),
            Parameter("alphas", self.alphas),
            Parameter("zpmin", self.zpmin),
            Parameter("zpmax", self.zpmax),
            Parameter("Kqf", self.Kqf),
            Parameter("betat", self.betat),
            Parameter("betas", self.betas),
            Parameter("zqmin", self.zqmin),
            Parameter("zqmax", self.zqmax),
            Parameter("Tpq", self.Tpq),
        ]


# We next define a few visualizations, which is anything that produces .txt or
# .pdf files (or any output) based on RAMSES' raw output. Any child of the
# Visualization class must have a "name" attribute and a "generate" method. The
# interface takes care of the rest.


class Voltages(Visualization):
    """Voltages of all buses plotted against time."""

    name = "time_voltages"

    def generate(
        self, system: pf.System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        plt.figure(figsize=(10, 10))

        for bus in system.buses:
            data = extractor.getBus(bus.name)
            time = data.mag.time
            voltage = data.mag.value
            x, y = reduce(x=time, y=voltage, step=10, integral_tol=1e-1)
            plt.plot(time, voltage, label=bus.name)
            self.save_data(
                vis_dir,
                f"time_voltage_{bus.name}.txt",
                "Time (s), Voltage (pu)",
                x,
                y,
            )

        plt.legend(bbox_to_anchor=(1.1, 1))
        plt.xlabel("Time (s)")
        plt.ylabel("Voltage (pu)")
        plt.title("All voltages")
        self.save_figure(vis_dir=vis_dir, filename="time_voltage.pdf")


class LoadPowers(Visualization):
    """Powers of all loads against time."""

    name = "load_powers"

    def generate(
        self, system: pf.System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        plt.figure()

        for inj in system.injectors:
            if isinstance(inj, RestorativeLoad):

                # Extract data
                data = extractor.getInj(inj.name)
                P_old = data.P.value
                Q_old = data.Q.value
                t_old = data.P.time

                # Resample data
                step = 0.02
                t = np.arange(t_old[0], t_old[-1] + step, step)
                P = np.interp(t, t_old, P_old)
                Q = np.interp(t, t_old, Q_old)

                # Add plot
                plt.plot(
                    t,
                    P*system.base_MVA,
                    label=f"{inj.name}",
                )

                # Save data (10 decimal places)
                self.save_data(
                    vis_dir,
                    f"time_{self.name}_{inj.name}.txt",
                    "Time (s), P (pu), Q (pu)",
                    t,
                    P,
                    Q,
                    fmt="%.10f",
                )

        plt.title("Powers consumed by the loads")
        plt.xlabel("Time (s)")
        plt.ylabel("Active power (MW)")
        plt.legend()
        self.save_figure(vis_dir=vis_dir, filename="load_powers.pdf")


class MagnitudesAngles(Visualization):
    """Voltage magnitudes and angles plotted against time."""

    name = "magnitudes_angles"

    def generate(
        self, system: pf.System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        fig, (ax_mags, ax_angs) = plt.subplots(
            2, 1, sharex=True, figsize=(10, 10)
        )

        ax_mags.set_title("All voltage magnitudes")
        ax_mags.set_ylabel("Voltage (pu)")

        ax_angs.set_title("All voltage angles")
        ax_angs.set_xlabel("Time (s)")
        ax_angs.set_ylabel("Angle (deg)")

        for bus in system.buses:

            # Extract data
            data = extractor.getBus(bus.name)
            time_old = data.mag.time
            mag_old = data.mag.value
            pha_old = data.pha.value

            # Resample data
            step = 0.02
            time = np.arange(time_old[0], time_old[-1] + step, step)
            mag = np.interp(time, time_old, mag_old)
            pha = np.interp(time, time_old, pha_old)

            # Save raw data (10 decimal places)
            self.save_data(
                vis_dir,
                f"time_voltage_magnitudes_{bus.name}.txt",
                "Time (s), Voltage (pu)",
                time,
                mag,
                fmt="%.10f",
            )
            self.save_data(
                vis_dir,
                f"time_voltage_angles_{bus.name}.txt",
                "Time (s), Angle (deg)",
                time,
                pha,
                fmt="%.10f",
            )

            # Extract plots
            ax_mags.plot(time, mag, label=f"{bus.name}")
            ax_angs.plot(time, pha)

        ax_mags.legend(bbox_to_anchor=(1.1, 1.00))
        legend_names = [bus.name for bus in system.buses]

        # Save figure manually
        fig.savefig(os.path.join(vis_dir, "time_agnitudes_angles.pdf"))


# The rest of the program contains functions that define the system, define the
# basic settings of the experiment, and then implement the specifics; these
# functions are called sequentially in the __main__ block.


def get_IEEE_39() -> pf.System:
    """Return the (dynamic) IEEE 39 bus test system."""

    IEEE_39 = pf.System.import_ARTERE(
        filename=os.path.join(
            "IEEE39bus_old",
            "IEEE39bus_pf_asinPSAT.dat",
        ),
        system_name="IEEE 39 bus test system - by Georgia Pierrou",
        use_injectors=True,
    )

    if not IEEE_39.run_pf(tol=1e-9):
        raise RuntimeError("Power flow of 39-bus system does not converge!")

    # We save the slack power to check later that the power flow remained
    # invariant.
    S1 = IEEE_39.get_S_slack_MVA()

    dyn_file = filename = os.path.join(
        "IEEE39bus_old", "IEEE39bus_dynamic_restldload_asinPSAT.dat"
    )
    IEEE_39.import_dynamic_data(dyn_file)

    # We now replace the traditional loads by restorative loads.
    with open(dyn_file) as f:
        for line in f:
            words = line.strip().strip(";").split()
            if len(words) > 0 and words[0] == "RESTLD":
                bus = IEEE_39.get_bus(name=words[2])
                for inj in IEEE_39.bus_to_injectors[bus]:
                    if isinstance(inj, Load):
                        old_inj = inj
                        IEEE_39.remove_injector(inj)
                FP = float(words[3])
                FQ = float(words[4])
                parameters = [
                    bus,
                    FP,
                    FQ,
                    -old_inj.get_P(),
                    -old_inj.get_Q(),
                ] + [float(x) for x in words[7:]]
                new_inj = RestorativeLoad(*parameters)
                IEEE_39.store_injector(inj=new_inj)

    # As hinted above, we check that the power flow did not change.
    IEEE_39.run_pf(tol=1e-9)
    S2 = IEEE_39.get_S_slack_MVA()
    assert np.isclose(S1, S2), "Power flow changed!"

    return IEEE_39


def get_base_experiment(name: str, horizon: float) -> tuple[Experiment, pf.System]:
    """Return an initialized experiment with its basic settings."""

    exp = Experiment(name=name)

    system = get_IEEE_39()

    # We make the system believe that it's under a contingency so that it can
    # be acted on by controllers.
    system.force_contingency()

    exp.add_system("IEEE 39", system)

    exp.set_solver_and_horizon(
        solver_settings_dict={
            "max_h": 0.02 / 2,  # to allow disturbances every 0.02 s = 20 ms
            "min_h": 0.001,
        },
        horizon=horizon,
    )

    exp.set_RAMSES_settings(
        settings_dict={"SPARSE_SOLVER": "ma41", "NB_THREADS": 10},
    )

    for element in system.buses + system.injectors + system.generators:
        exp.add_observables(Observable(element, "all"))

    L = next(
        inj for inj in system.injectors if isinstance(inj, RestorativeLoad)
    )

    exp.add_visualizations(
        MagnitudesAngles(),
        LoadPowers(),
    )

    exp.disturbance_window = 10

    return exp, system


def generate_trajectories(traj_scale: float = 1.0) -> None:
    """Apply the stochastic trajectories."""

    horizon_new = 400

    exp, system = get_base_experiment("Trajs.", horizon=horizon_new)

    # Extract data
    data = scipy.io.loadmat(
        file_name=os.path.join("IEEE39bus", "rnd39loadreg_temp4.mat")
    )
    trajectories = data["rnd39load"]

    # Data features
    samples_old = trajectories.shape[1]
    step_old = 0.02
    horizon_old = samples_old * step_old

    # Desired features
    step_new = 0.02  # every 1 s, every 0.1 s; 0.001, 0.02
    samples_new = int(horizon_new / step_new)
    t_values = np.arange(0, horizon_new, step_new) + 1
    skip = int(step_new / step_old)

    dists = []

    loads = [
        inj for inj in system.injectors if isinstance(inj, RestorativeLoad)
    ]
    for i, load in enumerate(loads):

        # Extract trajectories for this load
        traj_dPL_perc = traj_scale * trajectories[i, :][::skip][:samples_new]
        traj_dQL_perc = (
            traj_scale * trajectories[len(loads) + i, :][::skip][:samples_new]
        )

        # Save those disturbances
        for t, dPL_perc, dQL_perc in zip(
            t_values, traj_dPL_perc, traj_dQL_perc
        ):
            dists += [
                # Active power
                Disturbance(
                    ocurrence_time=t,
                    object_acted_on=load,
                    par_name="P0",
                    par_value=dPL_perc,
                    duration=0.0,
                    units="%",
                ),
                # Reactive power
                Disturbance(
                    ocurrence_time=t,
                    object_acted_on=load,
                    par_name="Q0",
                    par_value=dQL_perc,
                    duration=0.0,
                    units="%",
                ),
            ]

    exp.add_disturbances(
        f"Stoc. {traj_scale:.1f}",
        *dists,
    )

    with Timer(f"Obtaining trajectories with {traj_scale=}"):
        exp.run(
            remove_trj=False, simulate_no_control=True, simulate_no_dist=False
        )


def extract_B() -> None:
    """Extract the susceptance matrix (imaginary part of Ybus)."""

    system = get_IEEE_39()

    # Write buses
    bus_numbers = np.array([float(bus.name) for bus in system.buses])
    ind = np.argsort(bus_numbers)
    bus_numbers.sort()

    np.savetxt(
        fname="bus_numbers.txt", fmt="%.0f", X=bus_numbers, delimiter=","
    )

    # Write admittance matrix
    B = np.imag(system.Y)
    B = B[ind][:, ind]

    np.savetxt(fname="B_matrix.txt", fmt="%.10f", X=B, delimiter=",")

def extract_J() -> None:

    system = get_IEEE_39()

    # Write buses
    bus_numbers = np.array([float(bus.name) for bus in system.buses])
    ind = np.argsort(bus_numbers)
    bus_numbers.sort()

    np.savetxt(
        fname="bus_numbers.txt", fmt="%.0f", X=bus_numbers, delimiter=","
    )

    # Write Jacobian matrix
    dS_dVm = system.build_dS_dV()[0]
    dQ_dVm = dS_dVm.imag
    dQ_dVm = dQ_dVm[ind][:, ind]

    np.savetxt(fname="JQV_matrix.txt", fmt="%.10f", X=dQ_dVm, delimiter=",")

if __name__ == "__main__":

    extract_B()
    extract_J()

    with open("operating_point.txt", "w") as f:
        f.write(get_IEEE_39().generate_table())

    exit()
    for k in np.arange(0.1, 1.1, 0.1):
        generate_trajectories(traj_scale=k)
