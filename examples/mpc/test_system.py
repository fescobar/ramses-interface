"""

"""

# Modules from the standard library
import os
import sys

# Modules from this repository
sys.path.append(os.path.join("..", "..", "src"))
from pf_static import StaticSystem

def get_fivebus(solve: bool = True) -> StaticSystem:
    """
    Return five-bus system from Duncan Glover, example 6.9.
    """

    sys = StaticSystem(
        name="Five-bus system from Duncan Glover, example 6.9.\n"
        "Results are reported in table 6.6."
    )

    # Add buses
    b1 = sys.add_slack(V_pu=1.0, base_kV=15, name="Bus 1")
    b2 = sys.add_PQ(PL=8, QL=2.8, base_kV=345, name="Bus 2")
    b3 = sys.add_PV(V_pu=1.05, PL=0.8 - 5.2, base_kV=15, name="Bus 3")
    b4 = sys.add_PQ(PL=0, QL=0, base_kV=345, name="Bus 4")
    b5 = sys.add_PQ(PL=0, QL=0, base_kV=345, name="Bus 5")

    # Add lines
    sys.add_line(
        from_bus=b2, to_bus=b4, R=0.009, X=0.1, total_B=1.72, name="Line 2-4"
    )
    sys.add_line(
        from_bus=b2, to_bus=b5, R=0.0045, X=0.05, total_B=0.88, name="Line 2-5"
    )
    sys.add_line(
        from_bus=b4,
        to_bus=b5,
        R=0.00225,
        X=0.025,
        total_B=0.44,
        name="Line 4-5",
    )

    # Add transformers
    sys.add_transformer(from_bus=b1, to_bus=b5, R=0.0015, X=0.02, name="T1-5")
    sys.add_transformer(from_bus=b3, to_bus=b4, R=0.00075, X=0.01, name="T3-4")

    # Possibly run a power flow
    if solve:
        sys.run_pf()

    return sys

if __name__ == "__main__":

    system = get_fivebus(solve=True)

    print(system.generate_table())
