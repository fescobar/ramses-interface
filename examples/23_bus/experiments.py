"""
This program runs a dynamic simulation of a 23-bus system from an EPRI tutorial.
"""

# Modules from the standard library
import os
import sys

# Modules from this repository
sys.path.append(os.path.join("..", "..", "src"))
from pf_dynamic import System
from experiment import Experiment
from sim_interaction import Disturbance, Observable
from records import (
    Bus,
    CONSTANT,  # exciter with constant field voltage
    Generator,
    GENERIC1,  # generic exciter
    HYDRO_GENERIC1,  # generic turbine-governor system for hydro-generators
    Injector,
    Load,
    Parameter,
    SYNC_MACH,  # synchronous machine
)
from visual import Visualization
from utils import Timer

# Other modules
import pyramses
import numpy as np
import matplotlib.pyplot as plt


class GFL(Injector):
    """
    Energy resources interfaced by a grid-following (GFL) converter.

    Parameter descriptions are from the file inj_Gfol.f90, while the default
    values are from Van Cutsem's .pdf file "Modelling of grid-following
    voltage source converters under the phasor approximation."

    Since the parameter Snom has no default, the order of the arguments of
    the __init__ method is not the same as the order of the parameters in
    the RAMSES file. If you're calling the __init__() with keyword
    arguments, this should not be a problem.

    Attributes:
        P0_MW: injected active power at t = 0 s (MW)
        Q0_Mvar: injected reactive power at t = 0 s (Mvar)
        Snom: nominal apparent power (MVA)
        Pnom: nominal active power (MW)
        R: resistance of series transformer (pu on Snom base)
        L: reactance of series transformer (pu on Snom base)
        Rc: virtual resistance used to control the voltage (pu on Snom base)
        Xc: virtual reactance used to control the voltage (pu on Snom base)
        Kpp: active power control: proportional gain (pu)
        Kip: active power control: integral gain (pu)
        T: time constant used to limit the rate of recovery of active power
            after a limitation (s)
        dIddt_min: min. speed of active power recovery (pu/s)
        dIddt_max: max. speed of active power recovery (pu/s)
        Kpv: voltage control: proportional gain (pu)
        Kiv: voltage control: integral gain (pu)
        tau: PLL response time (s)
        Vdc: DC voltage setpoint (pu)
        Vpll: voltage under which the PLL is frozen (pu)
        Imax: maximum current in the converter (pu on Snom base)
        Vs1: voltage threshold below which dynamic (reactive) voltage
            support is activated (pu)
        Vs2: voltage threshold at which full dynamic (reactive) voltage
            support is exploited (pu, Vs2 < Vs1)
        Iqmax: (upper and lower) limit on reactive current/power in normal
            operating conditions (pu)
        VQswitch: switch: 1 = voltage control, 0 = reactive power control

    """

    prefix: str = "INJEC GFol"

    def __init__(
        self,
        name: str,
        bus: Bus,
        P0_MW: float,
        Q0_Mvar: float,
        Snom: float,
        Pnom: float,
        R: float = 0.005,
        L: float = 0.15,
        Rc: float = 0.005,
        Xc: float = 0.15,
        Kpp: float = 0,
        Kip: float = 39,
        T: float = 0.01,
        dIddt_min: float = -999,
        dIddt_max: float = 0.5,
        Kpv: float = 0,
        Kiv: float = 80,
        tau: float = 0.05,
        Vdc: float = 1,
        Vpll: float = 0.3,
        Imax: float = 1.2,
        Vs1: float = 0.9,
        Vs2: float = 0.5,
        Iqmax: float = 0.3,
        VQswitch: float = 1,
    ) -> None:

        attributes = vars()
        for key in attributes:
            setattr(self, key, attributes[key])

    def get_P(self) -> float:
        """Constant P when computing the power flows."""

        return self.P0_MW

    def get_Q(self) -> float:
        """Constant Q when computing the power flows."""

        return self.Q0_Mvar

    def get_dP_dV(self) -> float:
        """Numerical derivative of get_P(), obtained analytically."""

        return 0

    def get_dQ_dV(self) -> float:
        """Numerical derivative of get_Q(), obtained analytically."""

        return 0

    def get_pars(self) -> list[Parameter]:
        """Return list of parameter values."""

        return [
            Parameter("bus", self.bus.name),
            Parameter("FP", 0),
            Parameter("FQ", 0),
            Parameter("P", self.get_P(), digits=10),
            Parameter("Q", self.get_Q(), digits=10),
            Parameter("R", self.R),
            Parameter("L", self.L),
            Parameter("Rc", self.Rc),
            Parameter("Xc", self.Xc),
            Parameter("Snom", self.Snom),
            Parameter("Pnom", self.Pnom),
            Parameter("Kpp", self.Kpp),
            Parameter("Kip", self.Kip),
            Parameter("T", self.T),
            Parameter("dIddt_min", self.dIddt_min),
            Parameter("dIddt_max", self.dIddt_max),
            Parameter("Kpv", self.Kpv),
            Parameter("Kiv", self.Kiv),
            Parameter("tau", self.tau),
            Parameter("Vdc", self.Vdc),
            Parameter("Vpll", self.Vpll),
            Parameter("Imax", self.Imax),
            Parameter("Vs1", self.Vs1),
            Parameter("Vs2", self.Vs2),
            Parameter("Iqmax", self.Iqmax),
            Parameter("VQswitch", self.VQswitch),
        ]


class GFM(Injector):
    """
    Energy resources interfaced by a grid-forming (GFM) converter.

    Parameter descriptions are from the file inj_GFOR.f90, while the default
    values are from Van Cutsem's .pptx file "Generic model of grid forming
    converter."

    Since the parameter Snom has no default, the order of the arguments of
    the __init__ method is not the same as the order of the parameters in
    the RAMSES file. If you're calling the __init__() with keyword
    arguments, this should not be a problem.

    Attributes:
        P0_MW: injected active power at t = 0 s (MW)
        Q0_Mvar: injected reactive power at t = 0 s (Mvar)
        Snom: nominal apparent power (MVA)
        Rpr: phase reactor resistance (pu)
        Lpr: phase reactor inductance (pu)
        r: ratio of ideal transformer (pu)
        H: inertia constant (s)
        RVSC: steady-state frequency droop
        Kp: proportional gain in active power control
        Imax: maximum current (pu)
        n: exponent involved in gamma
        Kc: gain of integral control of current
        L1: lower limit of integral control current (pu)
        L2: upper limit of limiter in current control (pu)

    """

    prefix: str = "INJEC GFOR2"

    def __init__(
        self,
        name: str,
        bus: Bus,
        P0_MW: float,
        Q0_Mvar: float,
        Snom: float,
        Rpr: float = 0.005,
        Lpr: float = 0.15,
        r: float = 1.04,
        H: float = 5.0,
        RVSC: float = 25.0,
        Kp: float = 0.01592,
        Imax: float = 1.1,
        n: float = 2.0,
        Kc: float = 100.0,
        L1: float = -0.01,
        L2: float = 0.9,
    ) -> None:

        attributes = vars()
        for key in attributes:
            setattr(self, key, attributes[key])

    def get_P(self) -> float:
        """Constant P when computing the power flows."""

        return self.P0_MW

    def get_Q(self) -> float:
        """Constant Q when computing the power flows."""

        return self.Q0_Mvar

    def get_dP_dV(self) -> float:
        """Numerical derivative of get_P(), obtained analytically."""

        return 0

    def get_dQ_dV(self) -> float:
        """Numerical derivative of get_Q(), obtained analytically."""

        return 0

    def get_pars(self) -> list[Parameter]:
        """Return list of parameter values."""

        return [
            Parameter("bus", self.bus.name),
            Parameter("FP", 0),
            Parameter("FQ", 0),
            Parameter("P", self.get_P(), digits=10),
            Parameter("Q", self.get_Q(), digits=10),
            Parameter("Rpr", self.Rpr),
            Parameter("Lpr", self.Lpr),
            Parameter("r", self.r),
            Parameter("Snom", self.Snom),
            Parameter("H", self.H),
            Parameter("RVSC", self.RVSC),
            Parameter("Kp", self.Kp),
            Parameter("Imax", self.Imax),
            Parameter("n", self.n),
            Parameter("Kc", self.Kc),
            Parameter("L1", self.L1),
            Parameter("L2", self.L2),
        ]


def get_machine(name: str, bus: Bus, Snom_MVA: float) -> SYNC_MACH:

    return SYNC_MACH(
        name=name,
        bus=bus,
        Snom_MVA=Snom_MVA,
        Pnom_MW=0.9 * Snom_MVA,
        H=SYN_H,
        D=0.0,
        IBRATIO=2.05,
        model="XT",
        Xl=0.15,
        Xd=1.1,
        Xdp=0.25,
        Xdpp=0.2,
        Xq=0.7,
        Xqp="*",
        Xqpp=0.2,
        m=0.1,
        n=6.0257,
        Ra=0.0,
        Td0p=5.00,
        Td0pp=0.05,
        Tq0p="*",
        Tq0pp=0.1,
    )


def get_exciter() -> GENERIC1:
    """
    Return default exciter.

    Parameters correspond to generator 'g5' in the Nordic test system, a 250
    MW hydroelectric unit.
    """

    return GENERIC1(
        iflim=EXC_IFLIM,  # 1.8991, # raise so that there is enough headroom
        d=-0.1,
        f=0.0,
        s=1.0,
        k1=100.0,
        k2=-1.0,
        L1=-11.0,
        L2=10.0,
        G=EXC_G,
        Ta=10.0,  # set equal to Ta
        Tb=10.0,
        Te=0.10,
        L3=0.0,
        L4=4.0,
        SPEEDIN=1.0,
        KPSS=EXC_KPSS,  # set to 0 unless oscillations appear
        Tw=15.0,
        T1=0.2,
        T2=0.01,
        T3=0.2,
        T4=0.01,
        DVMIN=-0.1,
        DVMAX=0.1,
    )


def get_governor() -> HYDRO_GENERIC1:
    """
    Return default turbine/governor model.

    Parameters correspond to generator 'g5' in the Nordic test system, a 250
    MW hydroelectric unit.
    """

    return HYDRO_GENERIC1(
        sigma=0.04,
        Tp=2.0,
        Qv=0.0,
        Kp=GOV_KP,  # initial was 2.0, affects initial response
        Ki=GOV_KI,  # initial was 0.4, affects power recovery time
        Tsm=0.2,
        limzdot=0.1,
        Tw=GOV_Tw, # between 1 and 4, initial was 1.0
    )


def get_system() -> System:

    # Import system
    system = System.import_ARTERE(
        system_name="23-bus test system from EPRI's tutorial",
        filename="23_bus_system.txt",
    )

    # Fine-tune a few parameters from the network
    for line in system.lines:
        # Minimum allowed ratio is X/R = 1.
        line.X_pu = max(line.R_pu, line.X_pu)

    # For the transformer reactances, use integer values no smaller than 3
    for transformer in system.transformers:
        transformer.X_pu = max(
            np.ceil(100 * transformer.X_pu) / 100,
            0.03 * system.base_MVA / transformer.Snom_MVA,
        )

    system.run_pf(tol=1e-9, flat_start=True)
    S = system.get_S_slack_MVA()

    system.set_frequency(fnom=60)  # 60 Hz because it's an American system

    # Replace positive loads by injectors (exponential loads)
    for bus in system.buses:
        if bus.name in {"4", "5", "6", "7", "9", "25"}:
            load = Load(
                name=f"L{bus.name}",
                bus=bus,
                P0_MW=bus.PL_pu * system.base_MVA,  # positive means consumed
                Q0_Mvar=bus.QL_pu * system.base_MVA,  # positive means consumed
            )
            load.make_voltage_sensitive(alpha=1, beta=2)
            system.store_injector(load)
            bus.PL_pu = 0
            bus.QL_pu = 0

    # Replace negative loads by converters (see classes defined above). The
    # following definitions might seem a bit clumsy, but I think that they
    # facilitate switching between GFL and GFM, as only the dictionaries have
    # to be modified.

    get_GFM = GFM

    def get_GFL(
        name: str,
        bus: Bus,
        P0_MW: float,
        Q0_Mvar: float,
        Snom: float,
    ) -> GFL:
        return GFL(
            name=name,
            bus=bus,
            P0_MW=P0_MW,
            Q0_Mvar=Q0_Mvar,
            Snom=Snom,
            Pnom=0.9 * Snom,  # hard coded
        )

    CAPACITIES_MVA = {
        "13": 150,
        "19": 100,
        "22": 100,
    }
    CONTROL_MODES = {
        "13": "GFL",
        "19": "GFL",
        "22": "GFL",
    }

    for bus in system.buses:
        if bus.name in CAPACITIES_MVA:
            converter = (
                get_GFM if CONTROL_MODES[bus.name] == "GFM" else get_GFL
            )(
                name=f"{CONTROL_MODES[bus.name]}{bus.name}",
                bus=bus,
                P0_MW=-bus.PL_pu * system.base_MVA,  # minus means injected
                Q0_Mvar=-bus.QL_pu * system.base_MVA,  # minus means injected
                Snom=CAPACITIES_MVA[bus.name],
            )
            system.store_injector(converter)
            bus.PL_pu = 0
            bus.QL_pu = 0

    # Make sure that the power flow did not change
    system.run_pf(tol=1e-9)
    assert np.isclose(S, system.get_S_slack_MVA()), "Power flow changed!"

    # We now define the synchronous machines' constructive parameters, as well
    # as their controllers.

    SG_3 = system.get_generator(name="G3")
    SC_14 = system.get_generator(name="G14")
    SC_23 = system.get_generator(name="G23")
    SC_24 = system.get_generator(name="G24")

    # For simplicity, the constructive parameters of all machines are
    # considered equal (except of course for the nominal apparent power). See
    # the function get_machine().

    SG_3.machine = get_machine(name=SG_3.name, bus=SG_3.bus, Snom_MVA=SYN_SNOM_MVA)
    SC_14.machine = get_machine(name=SC_14.name, bus=SC_14.bus, Snom_MVA=SC_SNOM_MVA)
    SC_23.machine = get_machine(name=SC_23.name, bus=SC_23.bus, Snom_MVA=SC_SNOM_MVA)
    SC_24.machine = get_machine(name=SC_24.name, bus=SC_24.bus, Snom_MVA=SC_SNOM_MVA)

    # As is typical, synchronous condensers have a constant, negative, near
    # zero torque input, and hence their turbine-governor model is the one
    # called CONSTANT; the (only) synchronous machine that is operating as a
    # generator has, on the other hand, a turbine-governor model that is
    # typical for hydropower plants, called HYDRO_GENERIC1. All machines have
    # an excitation system called GENERIC1. For simplicity, the parameteres of
    # all these controllers have been defined in the functions get_exciter()
    # and get_governor().

    SG_3.exciter = get_exciter()
    SC_14.exciter = get_exciter()
    SC_23.exciter = get_exciter()
    SC_24.exciter = get_exciter()

    SG_3.governor = get_governor()
    SC_14.governor = CONSTANT()
    SC_23.governor = CONSTANT()
    SC_24.governor = CONSTANT()

    return system


# The following classes implement useful visualizations; a visualization is
# anything that results in an external file, such as PDFs, text files with
# data, and so on.


class VoltageMagnitudes(Visualization):
    """Voltage magnitudes of all buses plotted against time, in pu."""

    name = "voltage_magnitudes"

    def generate(
        self, system: System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        plt.figure()

        for bus in system.buses:
            data = extractor.getBus(bus.name)
            time_s = data.mag.time
            mag_pu = data.mag.value

            self.save_data(
                vis_dir,
                f"time_magnitudes_{bus.name}.txt",
                "Time (s), Voltage (pu)",
                time_s,
                mag_pu,
                fmt="%.10f",
            )

            plt.plot(time_s, mag_pu, label=f"{bus.name}")

        plt.legend()
        plt.xlabel("Time (s)")
        plt.ylabel("Voltage (pu)")
        self.save_figure(vis_dir=vis_dir, filename="time_magnitudes.pdf")


class Frequency(Visualization):
    """
    Frequency of the system plotted against time, in Hz.

    This visualization assumes that the system has at least one synchronous
    generator.
    """

    name = "frequency"

    def generate(
        self, system: System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        gen = system.generators[0]
        data = extractor.getSync(gen.name)
        COI_speed_pu = data.SC.value  # speed of COI reference (pu)
        f_Hz = (1 + COI_speed_pu) * system.fnom_Hz  # see p. 55 of STEPSS' docs
        time_s = data.SC.time

        plt.figure()
        plt.plot(time_s, f_Hz)
        plt.title("System frequency")
        plt.xlabel("Time (s)")
        plt.ylabel("Frequency (Hz)")
        self.save_figure(vis_dir=vis_dir, filename=f"{self.name}.pdf")


class PhasePortraits(Visualization):
    """
    Phase portraits in the delta-omega plane, with time being implicit.

    We first plot the phase portrait of each element separately, be it a VSC or
    a synchronous machine. We then plot all of them in the same plane.
    """

    name = "phase_portraits"

    def generate(
        self, system: System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        # We first define some functions that know how to extract delta and
        # omega in consistent units.

        def get_SM_portrait(
            element: Generator,
        ) -> tuple[np.ndarray, np.ndarray]:
            """Return values for synchronous generator."""

            data = extractor.getSync(element.name)
            delta_deg = data.A.value  # already in degrees
            omega_pu = data.S.value  # in pu
            omega_Hz = omega_pu * system.fnom_Hz  # array times scalar = array

            return delta_deg, omega_Hz

        def get_GFM_portrait(element: GFM) -> tuple[np.ndarray, np.ndarray]:
            """Return values for grid-forming converter."""

            data = extractor.getInj(element.name)
            delta_rad = data.deltam.value
            delta_deg = delta_rad * 180 / np.pi
            omega_pu = data.omegam.value  # from the .pptx, omegan is in pu
            omega_Hz = omega_pu * system.fnom_Hz

            return delta_deg, omega_Hz

        def get_GFL_portrait(element: GFL) -> tuple[np.ndarray, np.ndarray]:
            """Return values for grid-following converter."""

            data = extractor.getInj(element.name)
            P_MW = data.P_MW.value
            time_s = data.P_MW.time
            delta_rad = data.theta_pll.value
            delta_deg = delta_rad * 180 / np.pi
            omega_pu = data.w_pll.value  # by inspection, I know it's in pu
            omega_Hz = omega_pu * system.fnom_Hz

            return delta_deg, omega_Hz

        # We then generate the individual figures.

        def is_converter(inj) -> bool:
            """Is the injector inj a converter?"""
            return isinstance(inj, GFM) or isinstance(inj, GFL)

        data: list[tuple[str, np.ndarray, np.ndarray]] = []

        converters = [inj for inj in system.injectors if is_converter(inj)]
        for element in system.generators + converters:

            # The following is an attempt to emulate polymorphism. It might've
            # been easier if these were methods of each element...

            if isinstance(element, Generator):
                f = get_SM_portrait
            elif isinstance(element, GFM):
                f = get_GFM_portrait
            elif isinstance(element, GFL):
                f = get_GFL_portrait

            delta_deg, omega_Hz = f(element)
            plt.figure()
            plt.plot(delta_deg, omega_Hz, label=f"{element.name}")
            plt.xlabel("delta (deg)")
            plt.ylabel("omega (Hz)")
            plt.legend()

            self.save_figure(
                vis_dir=vis_dir,
                filename=f"{self.name}_{element.name}.pdf",
            )

            # We also save the data so that we don't have to fetch all the data
            # from scratch later on.
            data.append(
                (element.name, delta_deg, omega_Hz),
            )

        # Finally, we generate the figure with all the phase portraits.
        plt.figure()
        for name, delta_deg, omega_Hz in data:
            plt.plot(delta_deg, omega_Hz, label=name)
        plt.xlabel("delta (deg)")
        plt.ylabel("omega (Hz)")
        plt.legend()

        self.save_figure(
            vis_dir=vis_dir,
            filename=f"{self.name}_all.pdf",
        )


class GeneratorActivePower(Visualization):
    """
    Active power of all generators, plotted against time, in MW.

    The implementation is very similar to that of PhasePortraits() (in fact,
    it's almost a copy, which I don't think is a sin).
    """

    name = "generator_active_power"

    def generate(
        self, system: System, extractor: pyramses.extractor, vis_dir: str
    ) -> None:

        # We first define some functions that know how to extract delta and omega
        # in consistent units.

        def get_SM_power(element: Generator) -> tuple[np.ndarray, np.ndarray]:
            """Return values for synchronous generator."""

            data = extractor.getSync(element.name)
            P_MW = data.P.value
            time_s = data.P.time

            return time_s, P_MW

        def get_GFM_power(element: GFM) -> tuple[np.ndarray, np.ndarray]:
            """Return values for grid-forming converter."""

            data = extractor.getInj(element.name)
            P_MW = data.Pout.value
            time_s = data.Pout.time

            return time_s, P_MW

        def get_GFL_power(element: GFL) -> tuple[np.ndarray, np.ndarray]:
            """Return values for grid-following converter."""

            data = extractor.getInj(element.name)
            P_MW = data.P_MW.value
            time_s = data.P_MW.time

            return time_s, P_MW

        # We then generate the individual figures.

        def is_converter(inj) -> bool:
            """Is the injector inj a converter?"""
            return isinstance(inj, GFM) or isinstance(inj, GFL)

        data: list[tuple[str, np.ndarray, np.ndarray]] = []

        converters = [inj for inj in system.injectors if is_converter(inj)]
        for element in system.generators + converters:

            # The following is an attempt to emulate polymorphism. It might've
            # been easier if these were methods of each element...

            if isinstance(element, Generator):
                f = get_SM_power
            elif isinstance(element, GFM):
                f = get_GFM_power
            elif isinstance(element, GFL):
                f = get_GFL_power

            time_s, P_MW = f(element)
            plt.figure()
            plt.plot(time_s, P_MW, label=f"{element.name}")
            plt.xlabel("Time (s)")
            plt.ylabel("Generated active power (MW)")
            plt.legend()

            self.save_figure(
                vis_dir=vis_dir,
                filename=f"{self.name}_{element.name}.pdf",
            )

            # We also save the data so that we don't have to fetch all the data
            # from scratch later on.
            data.append(
                (element.name, time_s, P_MW),
            )

        # Finally, we generate the figure with all the phase portraits.
        plt.figure()
        for name, time_s, P_MW in data:
            plt.plot(time_s, P_MW, label=name)
        plt.xlabel("Time (s)")
        plt.ylabel("Generated active power (MW)")
        plt.legend()

        self.save_figure(
            vis_dir=vis_dir,
            filename=f"{self.name}_all.pdf",
        )


def get_base_experiment(
    name: str, horizon: float
) -> tuple[System, Experiment]:
    """
    The base experiment contains the following disturbances:

        - load increase of 5 % in all loads
        - short-circuit at bus 8
    """

    exp = Experiment(
        name=name,
        # Change the following path to your own DLL
        DLL_dir=(
            r"H:\RAMSES\URAMSES-3.40c\URAMSES-3.40c" r"\Release_intel_w64"
        ),
    )

    system = get_system()

    exp.add_system("23-bus", system)

    exp.set_solver_and_horizon(
        solver_settings_dict={
            "max_h": 0.02 / 2,  # half of the AC period should be fine
            "min_h": 0.001,
        },
        horizon=horizon,
    )

    exp.set_RAMSES_settings(
        settings_dict={
            "SPARSE_SOLVER": "ma41",
            "NB_THREADS": 10,  # change according to your hardware
            "S_BASE": 10,  # makes sense since we have lower voltage levels
            "OMEGA_REF": "COI",  # useful for drawing phase portraits
        },
    )

    for element in system.buses + system.injectors + system.generators:
        exp.add_observables(Observable(element, "all"))

    exp.add_visualizations(
        VoltageMagnitudes(),
        Frequency(),
        PhasePortraits(),
        GeneratorActivePower(),
    )

    load_changes = []
    for inj in system.injectors:
        if isinstance(inj, Load):
            load_changes += [
                Disturbance(
                    ocurrence_time=1.0,
                    object_acted_on=inj,
                    par_name="P0",
                    par_value=5,
                    duration=0.0,
                    units="%",
                )
            ]
    exp.add_disturbances("Load chg.", *load_changes)

    bus_8 = system.get_bus("8")
    T0_s = 1.0
    CLEARING_TIME_ms = 100
    exp.add_disturbances(
        "SC",
        Disturbance(T0_s, bus_8, "fault", 1e-4),  # 1e-4 is the fault X in pu
        Disturbance(T0_s + CLEARING_TIME_ms / 1000, bus_8, "clearance", None),
    )

    # Voltages are allowed to undergo severe transients only during the
    # following window. After that, severe under- resp. overvoltages will stop
    # the simulation.
    exp.disturbance_window = 10

    return system, exp


if __name__ == "__main__":

    # Changing the following (global) constants is not very elegant, but we'll
    # do it just to play around with some constants. If needed, loop over these
    # values.

    # Update 27.09.2024: The following values were selected so that the
    # response of the system is more realistic, especially when looking at the
    # frequency.

    SYN_SNOM_MVA = 250 # capacity of the generator, originally 150 MVA
    SC_SNOM_MVA = 150 # capacity of the SC, originally 50 MVA
    SYN_H = 4 # inertia constant, originally 6
    EXC_G = 120  # gain of the exciter
    EXC_IFLIM = 3.5  # ensure enough headroom for IF (starts at about 1.8)
    EXC_KPSS = 75  # in case oscillations appear (they do); otherwise 0
    GOV_KP = 15.0  # affects initial response, originally 2.0
    GOV_KI = 5.0  # affects recovery time of the active power, originally 0.4
    GOV_Tw = 1.0  # originally 1.0

    system, exp = get_base_experiment(
        name="23-bus",  # name of the created folder
        horizon=30,  # simulation horizon in seconds
    )

    with open("initial_conditions.txt", "w") as f:
        f.write(system.generate_table())

    with Timer("Simulating the disturbances from the tutorial"):
        exp.run(
            remove_trj=False,
            simulate_no_control=True,  # run case without system-wide controller
            simulate_no_dist=False,  # run case without any disturbance
        )
