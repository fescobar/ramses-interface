"""
Definition and tuning of the 25-bus system from the EPRI tutorial.
"""

# Modules from the standard library
import os
import sys

# Modules from this repository
sys.path.append(os.path.join("..", "..", "src"))
import pf_dynamic as pf
import records
import utils 

# Other modules
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint

# To ease the optimization and not have to worry about indices and so on, we'd
# like to overload some classes: Slack, PV, PQ, Branch, and System. We also
# define the weights globally, which is not elegant...

scale = 1.0
COST_SLACK: float = 1.0 * scale # 1e-12 * 100
COST_PV: float = 1.0 * scale # 1e-12 * 100
COST_PQ: float = 1.0 * 100 * scale # 1.0 * 100
COST_BRANCH: float = 1.0 * scale # 1e-12 * 100


# We also define globally the initial R, X, B, and X_TR values. It's not very
# elegant, but it works...
MAX_RL = 0.01
MAX_XL = 0.10
MAX_BL = 1.10
MAX_XT = 0.15
INITIAL_XL = MAX_XL / 2
INITIAL_RL = INITIAL_XL / 10 # MAX_RL / 2
INITIAL_BL = MAX_BL / 2 / 3
INITIAL_XT = MAX_XT / 2

class Slack(records.Slack):
    """
    The new methods are the ones that help us evaluate the cost function.

    In this class, as in the ones below, there is one method that sets the
    expected quantity (flows or voltages) and another one that penalizes the
    deviations from those expectations.
    """

    def set_expectation(self, P_inj_MW: float, Q_inj_Mvar: float) -> None:
        """Set the expected P and Q injections from the slack bus."""

        self.P_inj_MW = P_inj_MW
        self.Q_inj_Mvar = Q_inj_Mvar

    def get_cost_contribution(self) -> float:
        """Return contribution of the slack bus to the cost function."""

        # The 'computed' attributes are set by the update_bus_flows() method
        # defined below.
        delta_P = self.P_inj_MW_computed - self.P_inj_MW
        delta_Q = self.Q_inj_Mvar_computed - self.Q_inj_Mvar

        return COST_SLACK * (delta_P**2 + delta_Q**2)


class PV(records.PV):
    """
    The new methods are the ones that help us evaluate the cost function.
    """

    def set_expectation(self, Q_inj_Mvar: float) -> None:
        """Set expected Q injection from the PV bus."""

        self.Q_inj_Mvar = Q_inj_Mvar

    def get_cost_contribution(self) -> float:
        """Return contribution of the PV bus to the cost function."""

        delta_Q = self.Q_inj_Mvar_computed - self.Q_inj_Mvar

        return COST_PV * (delta_Q**2)


class PQ(records.PQ):
    """The new methods are the ones that help us evaluate the cost function."""

    def set_expectation(self, V_kV_expected: float) -> None:
        """Set expected voltage from the PV bus."""

        self.V_pu_expected = V_kV_expected / self.base_kV

    def get_cost_contribution(self) -> float:
        """Return contribution of the PQ bus to the cost function."""

        delta_V = self.V_pu_expected - self.V_pu

        return COST_PQ * (delta_V**2)


class Branch(records.Branch):
    """
    The class Branch will require more modifications than Bus.

    The reason for this asymmetry is that branches are precisely the elements
    we know less about. Furthermore, the branches have more parameters we can
    play with than buses: length, impedances, and so on.

    First, we implement the methods that help us evaluate the cost function.

    Then (possibly at a later point in time), we implement methods that help us
    infer the length of the lines as well as their primitive parameters.
    """

    def set_expectation(
        self,
        P_from_MW: float,
        Q_from_Mvar: float,
        P_to_MW: float,
        Q_to_Mvar: float,
    ) -> None:
        """Set expected flows entering the branch at both ends."""

        self.P_from_MW = P_from_MW
        self.Q_from_Mvar = Q_from_Mvar
        self.P_to_MW = P_to_MW
        self.Q_to_Mvar = Q_to_Mvar

    def get_cost_contribution(self, base_MVA: float) -> float:
        """Get contribution of the branch to the cost function."""

        # Recall that the get_pu_flows() method returns a tuple of the form
        # (P_from, Q_from, P_to, Q_to, P_losses) in pu, entering the branch.

        P_from_pu, Q_from_pu, P_to_pu, Q_to_pu = self.get_pu_flows()[:4]

        dP_from = self.P_from_MW - P_from_pu * base_MVA
        dQ_from = self.Q_from_Mvar - Q_from_pu * base_MVA
        dP_to = self.P_to_MW - P_to_pu * base_MVA
        dQ_to = self.Q_to_Mvar - Q_to_pu * base_MVA

        return COST_BRANCH * (dP_from**2 + dQ_from**2 + dP_to**2 + dQ_to**2)


class System(pf.System):
    """
    The new methods save as attributes the power flows exiting each bus and
    offer an interface between the decision variable 'x' and the attributes of
    the system.

    Update: it's actually useful to add methods to add the slack, PV, and PQ
    buses, as well as the lines and the transformers.
    """

    def define_slack(
        self,
        V_kV: float,
        base_kV: float,
        name: str,
        Snom_MVA: float,
        PL_MW: float = 0,
        QL_Mvar: float = 0,
    ) -> Slack:

        b = Slack(
            V_pu=V_kV/base_kV,
            theta_radians=0,
            PL_pu=PL_MW/self.base_MVA,
            QL_pu=QL_Mvar/self.base_MVA,
            G_pu=0,
            B_pu=0,
            base_kV=base_kV,
            bus_type="Slack",
            V_min_pu=np.nan,
            V_max_pu=np.nan,
            name=name,
        )

        self.store_bus(b)

        G = records.Generator(PG_MW=0.0, bus=b, name=f"G{name}")
        G.machine = records.SYNC_MACH(
            name=f"G{name}",
            bus=b,
            Snom_MVA=Snom_MVA,
            Pnom_MW=0.9*Snom_MVA,
            H=6.0,
            D=0.0,
            IBRATIO=2.05,
            model="XT",
            Xl=0.15,
            Xd=2.2,
            Xdp=0.3,
            Xdpp=0.2,
            Xq=2.0,
            Xqp=0.4,
            Xqpp=0.2,
            m=0.1,
            n=6.0257,
            Ra=0.,
            Td0p=7.00,
            Td0pp=0.05,
            Tq0p=1.5,
            Tq0pp=0.05,
        )

        self.store_generator(gen=G)

        return b

    def define_PV(
        self,
        V_kV: float,
        PG_MW: float,
        base_kV: float,
        name: str,
        Snom_MVA: float,
        PL_MW: float = 0,
        QL_Mvar: float = 0,
    ) -> PV:

        b = PV(
            V_pu=V_kV/base_kV,
            theta_radians=0,
            PL_pu=PL_MW/self.base_MVA,
            QL_pu=QL_Mvar/self.base_MVA,
            G_pu=0,
            B_pu=0,
            base_kV=base_kV,
            bus_type="PV",
            V_min_pu=np.nan,
            V_max_pu=np.nan,
            name=name,
        )

        self.store_bus(b)

        G = records.Generator(PG_MW=PG_MW, bus=b, name=f"G{name}")
        G.machine = records.SYNC_MACH(
            name=f"G{name}",
            bus=b,
            Snom_MVA=Snom_MVA,
            Pnom_MW=0.9*Snom_MVA,
            H=6.0,
            D=0.0,
            IBRATIO=2.05,
            model="XT",
            Xl=0.15,
            Xd=2.2,
            Xdp=0.3,
            Xdpp=0.2,
            Xq=2.0,
            Xqp=0.4,
            Xqpp=0.2,
            m=0.1,
            n=6.0257,
            Ra=0.,
            Td0p=7.00,
            Td0pp=0.05,
            Tq0p=1.5,
            Tq0pp=0.05,
        )

        self.store_generator(gen=G)

        return b

    def define_PQ(
        self, base_kV: float, name: str, PL_MW: float = 0, QL_Mvar: float = 0
    ) -> PQ:

        b = PQ(
            V_pu=1.0,
            theta_radians=0,
            PL_pu=PL_MW/self.base_MVA,
            QL_pu=QL_Mvar/self.base_MVA,
            G_pu=0,
            B_pu=0,
            base_kV=base_kV,
            bus_type="PQ",
            V_min_pu=np.nan,
            V_max_pu=np.nan,
            name=name,
        )

        self.store_bus(b)

        return b

    def define_line(
        self,
        from_bus: records.Bus,
        to_bus: records.Bus,
    ) -> Branch:

        def get_name():
            alphabet = [""] + list("ABCDEFGHI")
            for suffix in alphabet:
                name = (
                    f"{from_bus.name}-{to_bus.name}{len(suffix)*'-'}{suffix}"
                )
                if not any(b.name == name for b in self.branches):
                    return name
            # The following case is extremely unlikely in any system.
            raise RuntimeError("Ran out of names!")

        l = Branch(
            from_bus=from_bus,
            to_bus=to_bus,
            # The following parameters don't matter, as they will be
            # updated already in the first iteration of the optimizer
            # by assign_x().
            X_pu=INITIAL_XL,
            R_pu=INITIAL_RL,
            from_Y_pu=1j*INITIAL_BL/2,
            to_Y_pu=1j*INITIAL_BL/2,
            n_pu=1,
            branch_type="Line",
            Snom_MVA=99999,
            name=get_name(),
            sys=self,  # this is weird, but that's how it's coded!
        )

        self.store_branch(l)

        return l

    def define_transformer(
        self,
        from_bus: records.Bus,
        to_bus: records.Bus,
        Snom_MVA: float,
    ) -> Branch:

        def get_name():
            alphabet = [""] + list("ABCDEFGHI")
            for suffix in alphabet:
                name = (
                    f"{from_bus.name}-{to_bus.name}{len(suffix)*'-'}{suffix}"
                )
                if not any(b.name == name for b in self.branches):
                    return name
            # The following case is extremely unlikely in any system.
            raise RuntimeError("Ran out of names!")

        t = Branch(
            from_bus=from_bus,
            to_bus=to_bus,
            # The following parameters don't matter, as they will be
            # updated already in the first iteration of the optimizer
            # by assign_x()
            X_pu=INITIAL_XT,
            R_pu=0.00,
            from_Y_pu=0,
            to_Y_pu=0,
            n_pu=1,
            branch_type="Transformer",
            Snom_MVA=Snom_MVA,
            name=get_name(),
            sys=self,  # this is weird, but that's how it's coded!
        )

        self.store_branch(t)

        return t

    def get_PQ_MVA_out_of_bus(self, bus: records.Bus) -> tuple[float, float]:
        """Return P in MW and Q in Mvar being injected by a bus."""

        S_vector_pu = self.get_S_towards_network()
        i = self.buses.index(bus)
        S_MVA = S_vector_pu[i, 0] * self.base_MVA

        return S_MVA.real, S_MVA.imag

    def update_bus_flows(self) -> None:
        """Update the P in MW and Q in Mvar being injected by all buses."""

        for bus in self.buses:
            bus.P_inj_MW_computed, bus.Q_inj_Mvar_computed = (
                self.get_PQ_MVA_out_of_bus(bus)
            )

    def assign_x(self, x: np.ndarray) -> None:
        """Assign x, with the convention that the entries of x are already in pu."""

        # Here's where we enforce the meaning of x. If N_lin is the number of
        # lines in the system and N_tra is the number of transformers, then the
        # first 3 * N_lin elements of x correspond to R_pu, X_pu, B_pu of those
        # lines and the last N_tra correspond to the X_pu of those transformers.

        N_lin = len(self.lines)

        for i, line in enumerate(self.lines):

            R_pu = x[3 * i]
            X_pu = x[3 * i + 1]
            B_pu = x[3 * i + 2]
            # X_pu = x[2 * i]
            # B_pu = x[2 * i + 1]
            # R_pu = X_pu / 10


            line.R_pu: float = R_pu
            line.X_pu: float = X_pu
            total_Y = 0 + 1j * B_pu
            line.from_Y_pu = total_Y / 2
            line.to_Y_pu = total_Y / 2

        # For transformers, we also assume that X_pu is the system's base.

        for i, transformer in enumerate(self.transformers):

            offset = 3 * N_lin
            # offset = 2 * N_lin
            X_pu = x[i + offset]
            transformer.X_pu = X_pu

    def get_cost(self) -> float:
        if not self.run_pf(flat_start=True, tol=1e-6):
            return 0.3

        # If the power flow does converge, we just ask each element to report
        # on the cost. For the buses where we want to enforce flows, it's important
        # to update those first.

        self.update_bus_flows()

        total_cost = 0
        for element in self.buses + self.branches:
            # Note: the following if-else statement isn't nice; I wanted to
            # leverage polymorphism, but then I realized that the branches
            # somehow need to know how to convert their flows in pu to MW resp.
            # Mvar.
            if isinstance(element, Branch):
                contribution = element.get_cost_contribution(self.base_MVA)
            else:
                contribution = element.get_cost_contribution()

            total_cost += contribution

        # If you're curious about the contributions to the total cost, recall
        # that there are 4 flows for each branch (without exception), 1 unknown
        # voltage for each PQ bus, 2 unknown injections (P and Q) out of the
        # slack bus, and 1 unknown injection (Q) from each of the remaining
        # synchronous machines (generators and condensers), placed at PV buses.

        return total_cost


# To extend the existing information, we first have to organize it into a
# System. We define a function that takes care of that and returns us the
# 'base' system. This function is pretty cumbersome, but there's no way around
# this... sorry!


def get_initial_sys() -> pf.System:
    """
    Return the 25-bus system as defined in the EPRI tutorial.
    """

    system = System(name="25-bus system from EPRI's tutorial", base_MVA=100)

    # Define Slack
    bus_3 = system.define_slack(V_kV=18.0, name="3", base_kV=18.0, Snom_MVA=150)
    bus_3.set_expectation(P_inj_MW=46.7, Q_inj_Mvar=23.6)

    # Define PV buses (-0.2 MW of generated power means the synchronous
    # condensers are lowly-loaded motors).
    bus_14 = system.define_PV(V_kV=18, PG_MW=-0.2, base_kV=18, name="14", Snom_MVA=50)
    bus_14.set_expectation(Q_inj_Mvar=5.7)

    bus_23 = system.define_PV(V_kV=18, PG_MW=-0.2, base_kV=18, name="23", Snom_MVA=50) 
    bus_23.set_expectation(Q_inj_Mvar=19)

    bus_24 = system.define_PV(V_kV=18, PG_MW=-0.2, base_kV=18, name="24", Snom_MVA=50) 
    bus_24.set_expectation(Q_inj_Mvar=19)

    # Define PQ buses
    bus_4 = system.define_PQ(base_kV=230, name="4", PL_MW=5.9, QL_Mvar=4.8) 
    bus_4.set_expectation(V_kV_expected=226.5)

    bus_5 = system.define_PQ(base_kV=230, name="5", PL_MW=95.7+119.7, QL_Mvar=45.8+45.8) 
    bus_5.set_expectation(V_kV_expected=220.2)

    bus_6 = system.define_PQ(base_kV=230, name="6", PL_MW=70.4, QL_Mvar=22.9) 
    bus_6.set_expectation(V_kV_expected=224.9)

    bus_7 = system.define_PQ(base_kV=230, name="7", PL_MW=0.0, QL_Mvar=61.1) 
    bus_7.set_expectation(V_kV_expected=230.1)

    bus_8 = system.define_PQ(base_kV=230, name="8") 
    bus_8.set_expectation(V_kV_expected=229.7)

    bus_9 = system.define_PQ(base_kV=230, name="9", PL_MW=14.9, QL_Mvar=0.0) 
    bus_9.set_expectation(V_kV_expected=228.3)

    bus_10 = system.define_PQ(base_kV=230, name="10") 
    bus_10.set_expectation(V_kV_expected=231.9)

    bus_11 = system.define_PQ(base_kV=33, name="11") 
    bus_11.set_expectation(V_kV_expected=33.0)

    bus_12 = system.define_PQ(base_kV=33, name="12") 
    bus_12.set_expectation(V_kV_expected=33.0)

    bus_13 = system.define_PQ(base_kV=0.6, name="13", PL_MW=-100, QL_Mvar=8.4) 
    bus_13.set_expectation(V_kV_expected=0.6)

    bus_15 = system.define_PQ(base_kV=230, name="15") 
    bus_15.set_expectation(V_kV_expected=232.3)

    bus_16 = system.define_PQ(base_kV=230, name="16") 
    bus_16.set_expectation(V_kV_expected=230.5)

    bus_17 = system.define_PQ(base_kV=33, name="17") 
    bus_17.set_expectation(V_kV_expected=33.0)

    bus_18 = system.define_PQ(base_kV=33, name="18") 
    bus_18.set_expectation(V_kV_expected=33.0)

    bus_19 = system.define_PQ(base_kV=0.7, name="19", PL_MW=-75, QL_Mvar=-3) 
    bus_19.set_expectation(V_kV_expected=0.7)

    bus_20 = system.define_PQ(base_kV=33, name="20") 
    bus_20.set_expectation(V_kV_expected=33.0)

    bus_21 = system.define_PQ(base_kV=33, name="21") 
    bus_21.set_expectation(V_kV_expected=33.0)

    bus_22 = system.define_PQ(base_kV=0.6, name="22", PL_MW=-100, QL_Mvar=8.4)
    bus_22.set_expectation(V_kV_expected=0.6)

    bus_25 = system.define_PQ(base_kV=230, name="25", PL_MW=0.3, QL_Mvar=0.0)
    bus_25.set_expectation(V_kV_expected=231.9)

    # Maybe include some check that the line is being added between buses of
    # the same nominal voltage? And similarly, that the transformer joins buses
    # with a different voltage?

    # Define lines (no need to store in variables)
    # Checked
    system.define_line(from_bus=bus_4, to_bus=bus_5) \
          .set_expectation(
               P_from_MW=-25.0, 
               Q_from_Mvar=35.5, 
               P_to_MW=25.2, 
               Q_to_Mvar=-50.3,
           )

    # Checked
    system.define_line(from_bus=bus_4, to_bus=bus_6) \
          .set_expectation(
               P_from_MW=18.7, 
               Q_from_Mvar=-3.3, 
               P_to_MW=-18.7, 
               Q_to_Mvar=-11.6,
           )
          
    # Checked
    system.define_line(from_bus=bus_5, to_bus=bus_7) \
          .set_expectation(
               P_from_MW=-143.3, 
               Q_from_Mvar=7.3, 
               P_to_MW=150.6, 
               Q_to_Mvar=0.3,
           )

    # Checked 
    system.define_line(from_bus=bus_5, to_bus=bus_9) \
          .set_expectation(
               P_from_MW=-97.3, 
               Q_from_Mvar=-48.6, 
               P_to_MW=98.3, 
               Q_to_Mvar=41.4,
           )

    # Checked
    system.define_line(from_bus=bus_6, to_bus=bus_9) \
          .set_expectation(
               P_from_MW=-51.7, 
               Q_from_Mvar=-11.3, 
               P_to_MW=52.8, 
               Q_to_Mvar=-18.5,
           )

    # Checked 
    system.define_line(from_bus=bus_7, to_bus=bus_8) \
          .set_expectation(
               P_from_MW=122.6, 
               Q_from_Mvar=-17.4, 
               P_to_MW=-121.3, 
               Q_to_Mvar=11.3,
           )

    # Checked
    system.define_line(from_bus=bus_7, to_bus=bus_10) \
          .set_expectation(
               P_from_MW=-99.2, 
               Q_from_Mvar=-19.1, 
               P_to_MW=99.8, 
               Q_to_Mvar=-18.2,
           )

    # Checked
    system.define_line(from_bus=bus_7, to_bus=bus_15) \
          .set_expectation(
               P_from_MW=-99.2, 
               Q_from_Mvar=-19.1, 
               P_to_MW=99.7, 
               Q_to_Mvar=-13.5,
           )

    # Checked
    system.define_line(from_bus=bus_7, to_bus=bus_16) \
          .set_expectation(
               P_from_MW=-74.9, 
               Q_from_Mvar=-5.7, 
               P_to_MW=75.0, 
               Q_to_Mvar=-4.9,
           )

    # Checked
    system.define_line(from_bus=bus_8, to_bus=bus_9) \
          .set_expectation(
               P_from_MW=121.3, 
               Q_from_Mvar=-11.3, 
               P_to_MW=-119.5, 
               Q_to_Mvar=5.4,
           )

    # Checked
    system.define_line(from_bus=bus_10, to_bus=bus_15) \
          .set_expectation(
               P_from_MW=99.8, 
               Q_from_Mvar=-18.2, 
               P_to_MW=-99.7, 
               Q_to_Mvar=13.5,
           )

    # Checked
    system.define_line(from_bus=bus_10, to_bus=bus_25) \
          .set_expectation(
               P_from_MW=0.3, 
               Q_from_Mvar=0.0, 
               P_to_MW=-0.3, 
               Q_to_Mvar=0.0,
           )

    # Checked
    system.define_line(from_bus=bus_11, to_bus=bus_12) \
          .set_expectation(
               P_from_MW=-100, 
               # Q_from_Mvar=12.4, 
               Q_from_Mvar=11.9, 
               P_to_MW=100, 
               Q_to_Mvar=-11.9,
           )

    # Checked
    system.define_line(from_bus=bus_17, to_bus=bus_18) \
          .set_expectation(
               P_from_MW=-75, 
               Q_from_Mvar=0.2, 
               P_to_MW=75, 
               Q_to_Mvar=0.2,
           )

    # Checked
    system.define_line(from_bus=bus_20, to_bus=bus_21) \
          .set_expectation(
               P_from_MW=-100, 
               # Q_from_Mvar=12.4, 
               Q_from_Mvar=11.8, 
               P_to_MW=100, 
               Q_to_Mvar=-11.8,
           )

    # Define transformers (no need to store in variables)

    # Also check that 

    # Checked
    system.define_transformer(from_bus=bus_3, to_bus=bus_9, Snom_MVA=150) \
          .set_expectation(
               P_from_MW=46.7, 
               Q_from_Mvar=23.7, 
               P_to_MW=-46.7, 
               Q_to_Mvar=-22.9,
           )

    # Checked
    system.define_transformer(from_bus=bus_4, to_bus=bus_23, Snom_MVA=50) \
          .set_expectation(
               P_from_MW=0.2, 
               Q_from_Mvar=-18.5, 
               P_to_MW=-0.2, 
               Q_to_Mvar=18.9,
           )

    # Checked
    system.define_transformer(from_bus=bus_4, to_bus=bus_24, Snom_MVA=50) \
          .set_expectation(
               P_from_MW=0.2, 
               Q_from_Mvar=-18.5, 
               P_to_MW=-0.2, 
               Q_to_Mvar=18.9,
           )

    # Checked
    system.define_transformer(from_bus=bus_9, to_bus=bus_14, Snom_MVA=50) \
          .set_expectation(
               P_from_MW=0.2, 
               Q_from_Mvar=-5.6, 
               P_to_MW=-0.2, 
               Q_to_Mvar=5.7,
           )

    # Checked
    system.define_transformer(from_bus=bus_10, to_bus=bus_11, Snom_MVA=150) \
          .set_expectation(
               P_from_MW=-100, 
               Q_from_Mvar=18.4, 
               P_to_MW=100, 
               Q_to_Mvar=-12.6,
           )

    # Checked
    system.define_transformer(from_bus=bus_10, to_bus=bus_20, Snom_MVA=150) \
          .set_expectation(
               P_from_MW=-100, 
               Q_from_Mvar=18.4, 
               P_to_MW=100, 
               Q_to_Mvar=-12.6,
           )

    # Checked
    system.define_transformer(from_bus=bus_12, to_bus=bus_13, Snom_MVA=150) \
          .set_expectation(
               P_from_MW=-100, 
               Q_from_Mvar=11.8, 
               P_to_MW=100, 
               Q_to_Mvar=-8.4,
           )

    # Checked
    system.define_transformer(from_bus=bus_16, to_bus=bus_17, Snom_MVA=100) \
          .set_expectation(
               P_from_MW=-75, 
               Q_from_Mvar=5, 
               P_to_MW=75, 
               Q_to_Mvar=-0.3,
           )

    # Checked
    system.define_transformer(from_bus=bus_18, to_bus=bus_19, Snom_MVA=100) \
          .set_expectation(
               P_from_MW=-75, 
               Q_from_Mvar=-0.2, 
               P_to_MW=75, 
               Q_to_Mvar=3.0,
           )

    system.define_transformer(from_bus=bus_21, to_bus=bus_22, Snom_MVA=150) \
          .set_expectation(
               P_from_MW=-100, 
               Q_from_Mvar=11.8, 
               P_to_MW=100, 
               Q_to_Mvar=-8.4,
           )

    # L_13 = records.Load(name="GFL_13", bus=bus_13, P0_MW=-100, Q0_Mvar=8.4)
    # system.store_injector(L_13)

    # L_22 = records.Load(name="GFL_22", bus=bus_22, P0_MW=-100, Q0_Mvar=8.4)
    # system.store_injector(L_22)

    # L_19 = records.Load(name="GFL_19", bus=bus_19, P0_MW=-75, Q0_Mvar=-3.0)
    # system.store_injector(L_19)

    return system


# Finally, we define the scaffolding that will allow us to solve the
# optimization. Here we could take two approaches: either we overload the
# system class, or we define a pure function. I'll take the latter approach,
# since I don't see how we could profit from states (memory). Update: I did
# implement a class in the end...


def optimize_parameters(system: pf.System) -> pf.System:
    """Modify the line's parameters so that the power flows match the tutorial."""

    # One of the first things we need to think about is the number of
    # variables. For lines we consider R_pu, X_pu, and B_pu as decision
    # variables, whereas for transformers we only consider X_pu. All quantities
    # are in the system's base.
    N = 3 * len(system.lines) + 1 * len(system.transformers)
    # N = 2 * len(system.lines) + 1 * len(system.transformers)

    # Now we come to the initial guess. I can't think of one that would be
    # particularly advantageous, so let's try with a small constant. We'll
    # might have to fine-tune this as well...
    # By the way, we'll work with 1-D numpy arrays; there's no need to use
    # column vectors.

    N_lin = len(system.lines)
    N_tra = len(system.transformers)

    x0 = np.array(
        [INITIAL_RL, 
         INITIAL_XL, INITIAL_BL] * N_lin
        +
        [INITIAL_XT] * N_tra
    )

    # x0 = np.zeros(N) + 0.001 # fine-tune, maybe even differentiate for R, X and B

    # The only bounds we define on variables are the trivial ones: positivity.
    # The X/R ratios are taken care of by a NonlinearConstraint. We use a small
    # epsilon > 0 just in case.

    upper_bounds = (
        [MAX_RL, 
         MAX_XL, MAX_BL] * N_lin
        +
        [MAX_XT] * N_tra
    )

    epsilon = 1e-9
    bounds = [(epsilon, u) for u in upper_bounds]

    MIN_RATIO = 10 - 3
    MAX_RATIO = 10 + 3
    lb = MIN_RATIO * np.ones(N_lin)
    ub = MAX_RATIO * np.ones(N_lin)

    constraints = [
        # See below for the definition of XR_ratio
        NonlinearConstraint(fun=lambda x: XR_ratio(x, system), lb=lb, ub=ub)
    ]

    # We're now ready to run the optimization:
    res = minimize(
        # See below for the definition of cost
        fun=lambda x: cost(x, system),
        x0=x0,
        bounds=bounds,
        constraints=constraints,
        method="Powell",
        options={"disp": True, "maxiter": 100},
        # callback=lambda x: print(min(x), max(x)),
    )
    x_star = res.x

    # We finally assign the optimal parameters to the system and return this
    # system. 

    system.assign_x(x=x_star)

    return system


# You might've noticed that we used an auxiliary function to define the
# objective function and the constraint, required by scipy's optimizer. We
# define those below.


def cost(x: np.ndarray, system: pf.System) -> float:
    """
    Return the 'badness' of the parameters contained in x after solving system.

    While this function has two arguments, we'll get rid of the second one
    (system) using a lambda expression. Don't worry!
    """

    # It's in this function and in the following one that we define what the
    # entries of x really mean. To make slicing easier, let's say that the
    # first entries correspond to lines, while the last ones correspond to
    # transformers. Furthermore, the parameters of the lines are ordered as R_pu,
    # X_pu, B_pu.

    # Update: since I delegated the evaluation to a method, it's actually that
    # method the one that enforces an interpretation for x. One reason for that
    # delegation is that methods are somehow 'closer' to the system, and this
    # makes computing the cost easier. The implementation here is trivial,
    # almost tautological.

    system.assign_x(x=x)

    cost =  system.get_cost()

    # print(min(x), max(x), cost)

    return cost


def XR_ratio(x: np.ndarray, system: System) -> np.ndarray:
    """
    Return the X/R ratios for lines.
    """

    # Given the ordering specified in the previous function, we can extract
    # the resistances using
    N_lin = len(system.lines)
    R_pu = x[: 3 * N_lin][::3]
    # and the reactances using
    X_pu = x[1 : 1 + 3 * N_lin][::3]

    # The X/R ratios are simply an element-wise division, and we're done. We
    # don't really care about the ordering of the branches, since the constrain
    # is applied to all of them equally. Furthermore, since we're dealing with
    # ratios, we don't care about the units of R and X (but they are in pu).

    # Note: we might need to constrain the transformers' parameters as well,
    # which would require using the base_MVA attribute. Let's hope that the
    # optimizer knows how to achieve reasonable values by immitating the
    # (hopefully) reasonable power flows from the tutorial.

    return X_pu / R_pu


if __name__ == "__main__":

    system = get_initial_sys()
    system.run_pf(tol=1e-9)

    with utils.Timer("Parameter optimization"):
        system = optimize_parameters(system)

    # After this point, you're free do to whatever you want with the system!

    # Modify the active power of the slack
    G = system.bus_to_generators[system.slack][0]
    G.PG_MW = system.get_S_slack_MVA().real

    # Test the exporting and importing of systems
    FILENAME = "second_optimization.dat"
    system.export_to_ARTERE(header="", filename=FILENAME)
    system_2 = System.import_ARTERE(
        filename=FILENAME,
        system_name="25-bus system from EPRI's tutorial",
        base_MVA=system.base_MVA,
    )
    system_2.run_pf(tol=1e-9)

    # Modify synchronous machines


    # Add the grid-forming and grid-following converters (and restore powers)


